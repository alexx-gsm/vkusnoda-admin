import React, { Suspense } from 'react'
import Routes from '../../routes'
// context
import { AppContextProvider } from '../../contexts/app'
// components
import LinearIndeterminate from '../../components/LinearIndeterminate'
import AppHeader from '../../components/AppHeader'
// styles
import useStyles from './styles'

function Admin() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <AppContextProvider>
                <AppHeader />

                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    <Suspense fallback={<LinearIndeterminate />}>
                        <Routes />
                    </Suspense>
                </main>
            </AppContextProvider>
        </div>
    )
}

export default Admin
