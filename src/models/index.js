import dish from './dish'
import courier from './courier'
import client from './client'
import category from './category'
import uom from './uom'
import menu from './menu'
import output from './output'
import order from './order'

export default [dish, courier, client, category, uom, menu, output, order]
