import { getDayInterval } from '../../helpers/queryTemplates'

export default {
    model: 'Order',
    eventName: 'orders',
    autoload: true,
    options: {
        date: getDayInterval(new Date()),
    },
}
