import models from '../models'

export default [
    {
        url: '/',
        exact: true,
        component: 'Dashboard',
    },
    ...models.reduce((acc, { model, eventName }) => {
        return [
            ...acc,
            {
                url: `/${eventName}/edit/:_id?`,
                component: `${model}/${model}Edit`,
            },
            {
                url: `/${eventName}`,
                component: `${model}/${model}List`,
            },
        ]
    }, []),
]
