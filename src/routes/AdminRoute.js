import React, { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom'
// context
import { AuthContext } from '../contexts/auth'

const AdminRoute = ({ component: Component, ...rest }) => {
    const [{ isLoading, isLoggedIn, currentUser }] = useContext(AuthContext)

    if (isLoading) {
        return 'Loading'
    }

    if (!isLoggedIn) {
        return <Redirect to='/login' />
    }

    return (
        <Route
            {...rest}
            render={(props) =>
                isLoggedIn && currentUser.role === 'admin' ? (
                    <Component {...props} />
                ) : (
                    <Redirect to='/' />
                )
            }
        />
    )
}

export default AdminRoute
