import React, { lazy } from 'react'
import { Switch, Redirect } from 'react-router-dom'
import AdminRoute from './AdminRoute'
import routes from './adminRoutes'

function Routes() {
    return (
        <Switch>
            {routes &&
                routes
                    .filter(({ title }) => title !== 'divider')
                    .map(({ exact, url, component }) => (
                        <AdminRoute
                            key={url}
                            exact={exact}
                            path={url}
                            component={lazy(() => import(`../pages/${component}`))}
                        />
                    ))}
            <Redirect to='/' />
        </Switch>
    )
}

export default Routes
