import { useState, useEffect, useCallback } from 'react'
import axios from 'axios'
import { API_URL } from '../config/constants'

export default (url) => {
    const [isLoading, setIsLoading] = useState(false)
    const [response, setResponse] = useState(null)
    const [error, setError] = useState(null)
    const [options, setOptions] = useState({})

    const doFetch = useCallback((options) => {
        setOptions(options)
        setIsLoading(true)
    }, [])

    useEffect(() => {
        const { token = '', ...restOptions } = options
        const requestOptions = {
            ...restOptions,
            ...{
                headers: {
                    authorization: token,
                },
            },
        }
        if (!isLoading) return

        axios(API_URL + url, requestOptions)
            .then((res) => {
                setResponse(res.data)
                setIsLoading(false)
            })
            .catch((err) => {
                setError(err)
                setIsLoading(false)
            })
    }, [isLoading, options, url])

    return [{ isLoading, response, error }, doFetch]
}
