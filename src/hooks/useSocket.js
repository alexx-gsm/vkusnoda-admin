import openSocket from 'socket.io-client'
import { useState, useCallback } from 'react'
import { URL } from '../config/constants'

const socket = openSocket(URL)

export default () => {
    const [isConnected, setIsConnected] = useState(false)

    const doConnect = useCallback((token) => {
        socket
            .emit('authenticate', { token })
            .on('authenticated', () => {
                setIsConnected(true)
            })
            .on('unauthorized', (msg) => console.log('Unauthorized: ' + JSON.stringify(msg.data)))
            .on('disconnect', () => {
                setIsConnected(false)
                socket.emit('authenticate', { token })
            })
    }, [])

    return [{ socket, isConnected }, doConnect]
}
