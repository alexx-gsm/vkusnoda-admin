import React from 'react'
import ReactDOM from 'react-dom'
// context
import { AuthContextProvider } from './contexts/auth'
// components
import CurrentUserChecker from './components/CurrentUserChecker'
import App from './components/App'
// styles
import './index.css'

import * as serviceWorker from './serviceWorker'

ReactDOM.render(
    <AuthContextProvider>
        <CurrentUserChecker>
            <App />
        </CurrentUserChecker>
    </AuthContextProvider>,
    document.getElementById('root')
)

serviceWorker.unregister()
