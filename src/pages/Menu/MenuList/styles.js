import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Container: {
        padding: 0,
        marginLeft: 0,
    },
    Toolbar: {
        backgroundColor: theme.palette.grey[100],
        paddingLeft: theme.spacing(2),
    },
    AddButton: {
        color: theme.palette.secondary.dark,
    },
}))
