import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    TextRowTitle: {
        borderBottom: `1px dotted ${theme.palette.divider}`,
        fontWeight: theme.typography.fontWeightMedium,
    },
    TextNumber: {
        width: '90px',
    },
    TextInput: {
        textAlign: 'right',
    },
    TextNumberDefalt: {
        width: '55px',
        color: theme.palette.grey[600],
    },
    UomNumber: {
        width: '50px',
    },
}))
