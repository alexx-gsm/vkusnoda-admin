import React, { useState } from 'react'
import isEmpty from '../../../../helpers/is-empty'
// @material-ui
import { Grid, Divider } from '@material-ui/core'
import { Typography, TextField, InputAdornment } from '@material-ui/core'
import { IconButton, MenuItem, Select, Menu } from '@material-ui/core'
import { FormControl, FormHelperText, ListItemIcon } from '@material-ui/core'
// @material-icons
import CloseIcon from '@material-ui/icons/Close'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import MoreVertIcon from '@material-ui/icons/MoreVert'
// styles
import useStyles from './styles'

function DishRow({ dish, uoms, errors, handleRowDelete, handleRowInput, groupLength, index }) {
    const classes = useStyles()
    const { _id, num, title, weight, price, uomId } = dish

    const [anchorEl, setAnchorEl] = useState(null)
    const open = Boolean(anchorEl)

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }

    return (
        <Grid container wrap='nowrap' spacing={2} alignItems='center'>
            <Grid item>
                <Typography variant='h5' />
                {num}.
            </Grid>
            <Grid item xs>
                <Typography
                    variant='subtitle1'
                    noWrap
                    classes={{
                        root: classes.TextRowTitle,
                    }}
                >
                    {title}
                </Typography>
            </Grid>
            <Grid item>
                <TextField
                    value={weight}
                    onChange={handleRowInput(_id)}
                    name='weight'
                    id='weight'
                    type='number'
                    margin='none'
                    required
                    error={Boolean(errors.weight)}
                    helperText={errors.weight}
                    InputProps={{
                        classes: {
                            input: classes.TextInput,
                        },
                    }}
                    classes={{
                        root: classes.TextNumber,
                    }}
                />
            </Grid>
            <Grid item>
                <FormControl
                    error={!isEmpty(errors.uom)}
                    classes={{
                        root: classes.UomNumber,
                    }}
                >
                    <Select
                        labelId='uoms'
                        id='uoms-select'
                        name='uomId'
                        value={uomId}
                        onChange={handleRowInput(_id)}
                    >
                        {uoms.map(({ _id, abbr }) => (
                            <MenuItem key={_id} value={_id}>
                                {abbr}
                            </MenuItem>
                        ))}
                    </Select>
                    {errors.uom && <FormHelperText>{errors.uom}</FormHelperText>}
                </FormControl>
            </Grid>
            <Grid item>
                <TextField
                    value={price}
                    onChange={handleRowInput(_id)}
                    name='price'
                    id='price'
                    type='number'
                    margin='none'
                    required
                    InputProps={{
                        endAdornment: <InputAdornment position='end'>руб</InputAdornment>,
                        classes: {
                            input: classes.TextInput,
                        },
                    }}
                    error={Boolean(errors.price)}
                    helperText={errors.price}
                    classes={{
                        root: classes.TextNumber,
                    }}
                />
            </Grid>
            <Grid item>
                <IconButton
                    aria-label='more'
                    aria-controls='more-actions'
                    aria-haspopup='true'
                    onClick={handleClick}
                    size='small'
                >
                    <MoreVertIcon />
                </IconButton>
                <Menu
                    id='more-actions'
                    anchorEl={anchorEl}
                    keepMounted
                    open={open}
                    onClose={handleClose}
                    PaperProps={{
                        style: {
                            width: 200,
                        },
                    }}
                >
                    <MenuItem onClick={handleClose} disabled={index === 0}>
                        <ListItemIcon>
                            <ArrowUpwardIcon fontSize='small' />
                        </ListItemIcon>
                        <Typography>Вверх</Typography>
                    </MenuItem>
                    <MenuItem onClick={handleClose} disabled={index + 1 === groupLength}>
                        <ListItemIcon>
                            <ArrowDownwardIcon fontSize='small' />
                        </ListItemIcon>
                        <Typography>Вниз</Typography>
                    </MenuItem>
                    <Divider />
                    <MenuItem onClick={handleRowDelete(_id)}>
                        <ListItemIcon>
                            <CloseIcon size='small' color='error' aria-label='delete' />
                        </ListItemIcon>
                        <Typography color='error'>Удалить</Typography>
                    </MenuItem>
                </Menu>
            </Grid>
        </Grid>
    )
}

export default DishRow
