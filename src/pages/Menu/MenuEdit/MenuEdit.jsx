import React, { useState, useEffect, useContext } from 'react'
import { Redirect, useParams } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
import sortDishCategories from '../../../helpers/sortDishCategories'
// context
import { AppContext } from '../../../contexts/app'
// hook
import useSocket from '../../../hooks/useSocket'
// @material-ui
import { Paper, Box, Container, Grid } from '@material-ui/core'
import { Button } from '@material-ui/core'
import { Typography, TextField } from '@material-ui/core'
import { FormControlLabel } from '@material-ui/core'
import { Switch } from '@material-ui/core'
// @material-lab
import Autocomplete from '@material-ui/lab/Autocomplete'
// components
import FormHeader from '../../../components/FormHeader'
import FormFooter from '../../../components/FormFooter'
import DateSelect from '../../../components/DateSelect'
import DishRow from './DishRow/DishRow'
// styles
import useStyles from './styles'

import initialItem from './initialItem'

const baseUrl = '/menus'

function MenuEdit() {
    const classes = useStyles()
    const [{ socket }] = useSocket()

    /** form data */
    const [data, setData] = useState(initialItem)

    /** fetch data from context */
    const [{ menus, dishes, uoms, categories }] = useContext(AppContext)

    /** fetch item by '_id' */
    const { _id } = useParams()
    useEffect(() => {
        if (!_id || isEmpty(menus)) {
            return
        }

        const item = menus.find((i) => i._id === _id)
        setData(item)
    }, [_id, menus])

    /** populate autocomplete's options */
    const [_dishes, setDishes] = useState({})
    useEffect(() => {
        if (isEmpty(dishes)) {
            return
        }
        setDishes({
            options: dishes,
            getOptionLabel: (option) => option.title,
        })
    }, [dishes, setDishes])

    /** form handlers */
    const handleInput = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        })
    }

    const handleChange = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.checked,
        })
    }

    const handleDateChange = (date) => {
        setData({
            ...data,
            date,
        })
    }

    const handleAutocomplete = (e, val) => {
        setData({
            ...data,
            dish: val ? val._id : null,
        })
    }

    const getDish = (_id) => {
        const dish = dishes.find((d) => d._id === _id)

        return dish ? dish : {}
    }

    const handleAddItem = () => {
        let num = 1

        const sortedDishes = sortDishCategories(categories).reduce((acc, category) => {
            const categoriedDishes = [...data.dishes, getDish(data.dish)].filter(
                ({ categoryId }) => categoryId === category._id
            )

            return [...acc, ...categoriedDishes.map((cd) => ({ ...cd, num: num++ }))]
        }, [])

        setData({
            ...data,
            dishes: sortedDishes,
            dish: '',
        })
    }

    /** save/update item */
    const [isSaved, setIsSaved] = useState(false)
    const [errors, setErrors] = useState({})

    /** submit handler */
    const handleSubmit = () => {
        socket.emit('save', { model: 'Menu', eventName: 'menus', data }, (res) =>
            res.errors ? setErrors(res.errors) : setIsSaved(true)
        )
    }

    if (isSaved) {
        return <Redirect to={baseUrl} />
    }

    const getOption = (id) => {
        if (isEmpty(_dishes)) {
            return null
        }

        const item = _dishes.options.find((item) => item._id === id)
        return item ? item : null
    }

    const handleRowInput = (_id) => (e) => {
        setData({
            ...data,
            dishes: data.dishes.map((dish) => {
                return dish._id === _id
                    ? {
                          ...dish,
                          [e.target.name]: e.target.value,
                      }
                    : dish
            }),
        })
    }

    const handleRowDelete = (_id) => () => {
        setData({
            ...data,
            dishes: data.dishes.filter((d) => d._id !== _id),
        })
    }

    return (
        <Container maxWidth='md' classes={{ root: classes.Container }}>
            <Paper variant='outlined'>
                <FormHeader title={data._id ? 'Меню' : 'Новое Меню'} />
                <Box className={classes.Wrapper}>
                    <Grid container alignItems='center' justify='space-between' spacing={2}>
                        <Grid item>
                            <DateSelect date={data.date} handleDateChange={handleDateChange} />
                        </Grid>
                        <Grid item xs={3}>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={data.isPeriodic}
                                        onChange={handleChange}
                                        name='isPeriodic'
                                        color='secondary'
                                    />
                                }
                                label={data.isPeriodic ? 'Мульти' : 'Дата'}
                            />
                        </Grid>
                    </Grid>
                </Box>

                <Paper classes={{ root: classes.Paper }} variant='outlined' square>
                    <Grid container spacing={2} alignItems='center'>
                        <Grid item xs={9}>
                            {!isEmpty(_dishes) && (
                                <Autocomplete
                                    {..._dishes}
                                    id='dish'
                                    autoComplete
                                    value={getOption(data.dish)}
                                    onChange={handleAutocomplete}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label='Блюдо'
                                            margin='none'
                                            fullWidth
                                            error={Boolean(errors.parent)}
                                            helperText={errors.parent}
                                        />
                                    )}
                                />
                            )}
                        </Grid>
                        <Grid item xs={3}>
                            <Button
                                onClick={handleAddItem}
                                disabled={isEmpty(data.dish)}
                                variant='contained'
                                color='secondary'
                                fullWidth
                            >
                                Добавить
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>

                {!isEmpty(data.dishes) &&
                    !isEmpty(dishes) &&
                    !isEmpty(uoms) &&
                    !isEmpty(categories) && (
                        <Box className={classes.Wrapper}>
                            {sortDishCategories(categories).map((category) => {
                                const categoriedDishes = data.dishes.filter(
                                    ({ categoryId }) => categoryId === category._id
                                )

                                return (
                                    !isEmpty(categoriedDishes) && (
                                        <Box key={category._id} classes={{ root: classes.Section }}>
                                            <Typography align='center' color='primary' variant='h5'>
                                                {category.title}
                                            </Typography>
                                            {categoriedDishes.map((dish, index) => (
                                                <DishRow
                                                    key={dish._id}
                                                    index={index}
                                                    dish={dish}
                                                    uoms={uoms}
                                                    errors={errors}
                                                    handleRowDelete={handleRowDelete}
                                                    handleRowInput={handleRowInput}
                                                    groupLength={categoriedDishes.length}
                                                />
                                            ))}
                                        </Box>
                                    )
                                )
                            })}
                        </Box>
                    )}

                <Box className={classes.Wrapper}>
                    <TextField
                        value={data.comment}
                        onChange={handleInput}
                        name='comment'
                        margin='normal'
                        id='comment'
                        label='Комментарий'
                        type='text'
                        fullWidth
                        multiline
                        variant='outlined'
                        error={Boolean(errors.comment)}
                        helperText={errors.comment}
                    />
                </Box>

                <FormFooter {...{ baseUrl, handleSubmit }} />
            </Paper>
        </Container>
    )
}

export default MenuEdit
