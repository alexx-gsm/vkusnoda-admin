import React, { useState, useEffect, useContext } from 'react'
import { Redirect, useParams } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
// context
import { AppContext } from '../../../contexts/app'
// hook
import useSocket from '../../../hooks/useSocket'
// @material-ui
import { Paper, Box, Container, Grid } from '@material-ui/core'
import { TextField } from '@material-ui/core'
import { CircularProgress, Select, MenuItem } from '@material-ui/core'
import { FormControl, FormControlLabel, FormHelperText } from '@material-ui/core'
import { Switch } from '@material-ui/core'
import { InputLabel, InputAdornment } from '@material-ui/core'
import { Autocomplete } from '@material-ui/lab'
// components
import FormHeader from '../../../components/FormHeader'
import FormFooter from '../../../components/FormFooter'
// styles
import useStyles from './styles'

import initialItem from './initialItem'

const baseUrl = '/dishes'
const rootCategoryTitle = 'Блюда'

function DishEdit() {
    const classes = useStyles()
    const [{ socket }] = useSocket()

    /** form data */
    const [data, setData] = useState(initialItem)

    /** fetch data from context */
    const [{ dishes, categories, uoms }] = useContext(AppContext)

    /** fetch item by '_id' */
    const { _id } = useParams()
    useEffect(() => {
        if (!_id || isEmpty(dishes)) {
            return
        }

        const item = dishes.find((i) => i._id === _id)
        setData(item)
    }, [_id, dishes])

    /** populate autocomplete's options */
    const [_categories, setCategories] = useState({})
    useEffect(() => {
        if (isEmpty(categories)) return
        const rootCategory = categories.find((c) => c.title === rootCategoryTitle)

        setCategories({
            options: categories.filter((c) => c.parent === rootCategory._id),
            getOptionLabel: (item) => item.title,
        })
    }, [categories, setCategories])

    /** form handlers */
    const handleInput = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        })
    }

    const handleChange = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.checked,
        })
    }

    const handleAutocomplete = (name) => (e, val) => {
        setData({
            ...data,
            [name]: val ? val._id : null,
        })
    }

    /** save/update item */
    const [isSaved, setIsSaved] = useState(false)
    const [errors, setErrors] = useState({})

    /** submit handler */
    const handleSubmit = () => {
        socket.emit('save', { model: 'Dish', eventName: 'dishes', data }, (res) =>
            res.errors ? setErrors(res.errors) : setIsSaved(true)
        )
    }

    if (isSaved) {
        return <Redirect to={baseUrl} />
    }

    if (isEmpty(_categories) || isEmpty(uoms)) {
        return <CircularProgress color='secondary' />
    }

    const getItem = (id, items) => {
        const item = items.options.find((item) => item._id === id)
        return item ? item : null
    }

    return (
        <Container maxWidth='sm' classes={{ root: classes.Container }}>
            <Paper variant='outlined'>
                <FormHeader title={data._id ? 'Блюдо' : 'Новое блюдо'} />

                <Box className={classes.Wrapper}>
                    <TextField
                        autoFocus
                        value={data.title}
                        onChange={handleInput}
                        name='title'
                        margin='normal'
                        id='title'
                        label='Название'
                        fullWidth
                        required
                        error={Boolean(errors.title)}
                        helperText={errors.title}
                        InputProps={{
                            classes: { input: classes.Title },
                        }}
                    />

                    <TextField
                        value={data.cardTitle}
                        onChange={handleInput}
                        name='cardTitle'
                        margin='normal'
                        id='cardTitle'
                        label='Короткое название'
                        fullWidth
                        required
                        error={Boolean(errors.cardTitle)}
                        helperText={errors.cardTitle}
                        InputProps={{
                            classes: { input: classes.Title },
                        }}
                    />

                    <Grid container alignItems='flex-end' spacing={2}>
                        <Grid item xs={6}>
                            <Autocomplete
                                {..._categories}
                                id='category'
                                autoComplete
                                value={getItem(data.categoryId, _categories)}
                                onChange={handleAutocomplete('categoryId')}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        fullWidth
                                        name='categoryId'
                                        label='Категория'
                                        margin='normal'
                                        required
                                        error={Boolean(errors.categoryId)}
                                        helperText={errors.categoryId}
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item container xs={6} justify='flex-end'>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={data.isComplex}
                                        onChange={handleChange}
                                        name='isComplex'
                                        color='primary'
                                    />
                                }
                                label='Составное'
                            />
                        </Grid>
                    </Grid>

                    <Grid container alignItems='center' spacing={2}>
                        <Grid item xs={4}>
                            <TextField
                                value={data.weight}
                                onChange={handleInput}
                                name='weight'
                                id='weight'
                                label='Вес'
                                type='number'
                                margin='none'
                                required
                                fullWidth
                                error={Boolean(errors.weight)}
                                helperText={errors.weight}
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <FormControl fullWidth error={!isEmpty(errors.uom)}>
                                <InputLabel id='uoms' required>
                                    UOM
                                </InputLabel>
                                <Select
                                    labelId='uoms'
                                    id='uoms-select'
                                    name='uomId'
                                    value={data.uomId}
                                    onChange={handleInput}
                                >
                                    {uoms.map(({ _id, abbr }) => (
                                        <MenuItem key={_id} value={_id}>
                                            {abbr}
                                        </MenuItem>
                                    ))}
                                </Select>
                                {errors.uomId && <FormHelperText>{errors.uomId}</FormHelperText>}
                            </FormControl>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                value={data.price}
                                onChange={handleInput}
                                name='price'
                                id='price'
                                label='Цена'
                                type='number'
                                margin='none'
                                required
                                fullWidth
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position='end'>руб</InputAdornment>
                                    ),
                                }}
                                error={Boolean(errors.price)}
                                helperText={errors.price}
                            />
                        </Grid>
                    </Grid>

                    <Box className={classes.TextWrapper}>
                        <TextField
                            value={data.composition}
                            onChange={handleInput}
                            name='composition'
                            margin='none'
                            id='composition'
                            label='Состав'
                            type='text'
                            fullWidth
                            multiline
                            rowsMax={3}
                            InputLabelProps={{ shrink: true }}
                        />
                    </Box>

                    <TextField
                        value={data.comment}
                        onChange={handleInput}
                        name='comment'
                        margin='none'
                        id='comment'
                        label='Комментарий'
                        type='text'
                        fullWidth
                        multiline
                        InputLabelProps={{ shrink: true }}
                        variant='outlined'
                    />
                </Box>
                <FormFooter {...{ baseUrl, handleSubmit }} />
            </Paper>
        </Container>
    )
}

export default DishEdit
