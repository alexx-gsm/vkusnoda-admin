import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Container: {
        padding: 0,
        marginLeft: 0,
    },
    Wrapper: {
        padding: theme.spacing(1, 2),
    },
    Title: {
        ...theme.typography.title,
    },
    TypoUpperCase: {
        textTransform: 'uppercase',
    },
    RadioWrapper: {
        margin: theme.spacing(2, 0),
        padding: theme.spacing(2),
        backgroundColor: theme.palette.grey[200],
    },
}))
