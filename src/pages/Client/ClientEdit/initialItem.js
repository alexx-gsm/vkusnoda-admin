export default {
    name: '',
    isGroup: false,
    organization: '',
    parent: '',
    acronym: '',
    phones: [],
    address: [],
    courier: '',
    payment: 'cash',
    comment: '',
    notes: [],
}
