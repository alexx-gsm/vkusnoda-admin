import React, { Fragment, useState, useEffect, useContext } from 'react'
import { Redirect, useParams } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
// context
import { AppContext } from '../../../contexts/app'
// hook
import useSocket from '../../../hooks/useSocket'
// @material-ui
import { Paper, Box, Container, Grid } from '@material-ui/core'
import { TextField } from '@material-ui/core'
import { FormControl, FormLabel, FormControlLabel, FormHelperText } from '@material-ui/core'
import { RadioGroup, Radio, Switch } from '@material-ui/core'
import { Select, MenuItem, InputLabel } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
// components
import FormHeader from '../../../components/FormHeader'
import FormFooter from '../../../components/FormFooter'
// styles
import useStyles from './styles'

import initialItem from './initialItem'

const baseUrl = '/clients'

function ClientEdit() {
    const classes = useStyles()
    const [{ socket }] = useSocket()
    /** form data */
    const [data, setData] = useState(initialItem)

    /** fetch data from context */
    const [{ clients, couriers }] = useContext(AppContext)

    /** fetch item by '_id' */
    const { _id } = useParams()
    useEffect(() => {
        if (!_id || isEmpty(clients)) {
            return
        }

        const item = clients.find((c) => c._id === _id)
        setData(item)
    }, [_id, clients])

    /** populate autocomplete's options */
    const [groups, setGroups] = useState({})
    useEffect(() => {
        setGroups({
            options: clients.filter((c) => c.isGroup),
            getOptionLabel: (option) => option.organization,
        })
    }, [clients, setGroups])

    /** form handlers */
    const handleInput = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        })
    }

    const handleInputPhones = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value.split(', '),
        })
    }

    const handleChange = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.checked,
        })
    }

    const handleAutocomplete = (e, val) => {
        setData({
            ...data,
            parent: val ? val._id : null,
        })
    }

    /** save/update category */
    const [isSaved, setIsSaved] = useState(false)
    const [errors, setErrors] = useState({})

    /** submit handler */
    const handleSubmit = () => {
        socket.emit(
            'save',
            {
                model: 'Client',
                eventName: 'clients',
                data,
                options: { hasNumber: true },
            },
            (res) => (res.errors ? setErrors(res.errors) : setIsSaved(true))
        )
    }

    const getGroup = (id) => {
        const item = groups.options.find((item) => item._id === id)
        return item ? item : null
    }

    if (isSaved) {
        return <Redirect to={baseUrl} />
    }

    return (
        <Container maxWidth='sm' classes={{ root: classes.Container }}>
            <Paper variant='outlined'>
                <FormHeader
                    title={
                        data.isGroup ? 'Группа' : `Клиент (${data.number ? data.number : 'new'})`
                    }
                />
                <Box className={classes.Wrapper}>
                    <Grid container justify='space-between' alignItems='center' spacing={2}>
                        <Grid item xs>
                            <TextField
                                value={data.organization}
                                onChange={handleInput}
                                name='organization'
                                margin='normal'
                                id='organization'
                                label='Организация'
                                type='text'
                                fullWidth
                                required
                                InputProps={{
                                    classes: { input: classes.TypoUpperCase },
                                }}
                                error={Boolean(errors.organization)}
                                helperText={errors.organization}
                            />
                        </Grid>
                        <Grid item>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={data.isGroup}
                                        onChange={handleChange}
                                        name='isGroup'
                                        color='primary'
                                    />
                                }
                                label='Группа'
                            />
                        </Grid>
                    </Grid>
                    {data.isGroup && !isEmpty(groups) ? (
                        <Autocomplete
                            {...groups}
                            id='groups'
                            autoComplete
                            disabled={isEmpty(groups)}
                            value={getGroup(data.parent)}
                            onChange={handleAutocomplete}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label='Группа'
                                    margin='normal'
                                    fullWidth
                                    error={Boolean(errors.parent)}
                                    helperText={errors.parent}
                                />
                            )}
                        />
                    ) : (
                        <Fragment>
                            <TextField
                                value={data.name}
                                onChange={handleInput}
                                name='name'
                                margin='normal'
                                id='name'
                                label='ФИО'
                                type='text'
                                fullWidth
                                required
                                autoFocus
                                InputProps={{
                                    classes: { input: classes.Title },
                                }}
                                error={Boolean(errors.name)}
                                helperText={errors.name}
                            />

                            {!isEmpty(groups) && (
                                <Autocomplete
                                    {...groups}
                                    id='groups'
                                    autoComplete
                                    value={getGroup(data.parent)}
                                    onChange={handleAutocomplete}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label='Группа'
                                            margin='normal'
                                            fullWidth
                                            error={Boolean(errors.parent)}
                                            helperText={errors.parent}
                                        />
                                    )}
                                />
                            )}
                            <TextField
                                value={data.phones.join(', ')}
                                onChange={handleInputPhones}
                                name='phones'
                                margin='normal'
                                id='phones'
                                label='Телефон'
                                type='text'
                                fullWidth
                                required
                                error={Boolean(errors.phones)}
                                helperText={errors.phones}
                            />
                            <TextField
                                value={data.address}
                                onChange={handleInput}
                                name='address'
                                margin='normal'
                                id='address'
                                label='Адрес'
                                type='text'
                                fullWidth
                                required
                                error={Boolean(errors.address)}
                                helperText={errors.address}
                            />
                            {!isEmpty(couriers) && (
                                <FormControl
                                    margin='normal'
                                    fullWidth
                                    error={!isEmpty(errors.courier)}
                                >
                                    <InputLabel id='couriers' required>
                                        Курьер
                                    </InputLabel>
                                    <Select
                                        labelId='couriers'
                                        id='couriers-select'
                                        name='courier'
                                        value={data.courier}
                                        onChange={handleInput}
                                    >
                                        {couriers.map(({ _id, title }) => (
                                            <MenuItem key={_id} value={_id}>
                                                {title}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                    {errors.courier && <FormHelperText>Error</FormHelperText>}
                                </FormControl>
                            )}
                            <Box classes={{ root: classes.RadioWrapper }}>
                                <FormControl>
                                    <FormLabel>Оплата</FormLabel>
                                    <RadioGroup
                                        aria-label='paymrnt'
                                        name='payment'
                                        value={data.payment}
                                        onChange={handleInput}
                                        row
                                    >
                                        <FormControlLabel
                                            value='cash'
                                            control={<Radio color='primary' />}
                                            label='Деньги'
                                        />
                                        <FormControlLabel
                                            value='card'
                                            control={<Radio color='primary' />}
                                            label='Карта'
                                        />
                                        <FormControlLabel
                                            value='account'
                                            control={<Radio color='primary' />}
                                            label='Счет'
                                        />
                                    </RadioGroup>
                                </FormControl>
                            </Box>
                            <TextField
                                value={data.comment}
                                onChange={handleInput}
                                name='comment'
                                margin='normal'
                                id='comment'
                                label='Комментарий'
                                type='text'
                                fullWidth
                                multiline
                                variant='outlined'
                                error={Boolean(errors.comment)}
                                helperText={errors.comment}
                            />
                        </Fragment>
                    )}
                </Box>
                <FormFooter {...{ baseUrl, handleSubmit }} />
            </Paper>
        </Container>
    )
}

export default ClientEdit
