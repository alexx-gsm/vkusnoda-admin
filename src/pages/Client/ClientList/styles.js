import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Container: {
        padding: 0,
        marginLeft: 0,
    },
    Paper: { paddingBottom: theme.spacing(2) },
    GridHeader: {
        backgroundColor: theme.palette.grey[100],
        padding: theme.spacing(2),
        marginBottom: theme.spacing(2),
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    Title: {
        color: theme.palette.secondary.dark,
        textTransform: 'uppercase',
        marginRight: 'auto',
    },
    SearchField: {
        marginRight: theme.spacing(2),
    },
    IconClear: {
        marginRight: -theme.spacing(1.5),
    },
    Toolbar: {
        backgroundColor: theme.palette.grey[100],
        paddingLeft: theme.spacing(2),
    },
    AddButton: {
        backgroundColor: theme.palette.secondary.dark,
        color: theme.palette.common.white,
    },
    root: {
        color: theme.palette.text.secondary,
        '&:focus > $content': {
            backgroundColor: `var(--tree-view-bg-color, ${theme.palette.grey[400]})`,
            color: 'var(--tree-view-color)',
        },
    },
    content: {
        color: theme.palette.text.secondary,
        borderTopRightRadius: theme.spacing(2),
        borderBottomRightRadius: theme.spacing(2),
        paddingRight: theme.spacing(1),
        fontWeight: theme.typography.fontWeightMedium,
        '$expanded > &': {
            fontWeight: theme.typography.fontWeightRegular,
        },
    },
    group: {
        marginLeft: 0,
        '& $content': {
            paddingLeft: theme.spacing(2),
        },
    },
    expanded: {},
    label: {
        fontWeight: 'inherit',
        color: 'inherit',
    },
    labelRoot: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0.5, 0),
    },
    labelIcon: {
        marginRight: theme.spacing(1),
    },
    labelText: {
        fontWeight: 'inherit',
        flexGrow: 1,
    },
}))
