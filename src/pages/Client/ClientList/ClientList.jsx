import React, { useState, useEffect, useContext } from 'react'
import { useHistory } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
// context
import { AppContext } from '../../../contexts/app'
// @material-ui
import { Container, Paper } from '@material-ui/core'
// @material-icon
import PersonIcon from '@material-ui/icons/Person'
import iconNotEmptyGroup from '@material-ui/icons/FolderShared'
import iconEmptyGroup from '@material-ui/icons/FolderSharedOutlined'
// components
import Header from '../../../components/common/Header'
import StyledTree from '../../../components/common/StyledTree/StyledTree'
// styles
import useStyles from './styles'

const baseUrl = '/clients'

function ClientList() {
    const classes = useStyles()

    const history = useHistory()

    const [filter, setFilter] = useState('')
    const [filteredItems, setFilteredItems] = useState([])

    const [{ clients }] = useContext(AppContext)

    useEffect(() => {
        if (isEmpty(filter) || isEmpty(clients)) {
            return setFilteredItems([])
        }

        const filteredItems = clients
            .filter((item) => !item.isGroup)
            .filter(
                (item) =>
                    item.name.toLowerCase().search(filter.toLowerCase()) !== -1 ||
                    item.phones.join(' ').toLowerCase().search(filter.toLowerCase()) !== -1 ||
                    item.address.toLowerCase().search(filter.toLowerCase()) !== -1
            )

        setFilteredItems(filteredItems)
    }, [filter, clients, setFilteredItems])

    const getTitle = (client) => {
        const group = !isEmpty(client.parent) ? clients.find((c) => c._id === client.parent) : ''

        return `${client.name}, (${client.phones.join(', ')}), ${group.organization}`
    }

    return (
        <Container maxWidth='sm' classes={{ root: classes.Container }}>
            <Paper classes={{ root: classes.Paper }}>
                <Header
                    title='Клиенты'
                    filter={filter}
                    setFilter={setFilter}
                    disabled={isEmpty(clients)}
                    handleClick={() => history.push(`${baseUrl}/edit/`)}
                />

                <StyledTree
                    items={clients}
                    isFilter={!isEmpty(filter)}
                    filteredItems={filteredItems}
                    FilteredItemIcon={PersonIcon}
                    ItemIcon={PersonIcon}
                    iconEmptyGroup={iconEmptyGroup}
                    iconNotEmptyGroup={iconNotEmptyGroup}
                    getTitle={getTitle}
                    handleClick={(_id) => () => history.push(`${baseUrl}/edit/${_id}`)}
                />
            </Paper>
        </Container>
    )
}

export default ClientList
