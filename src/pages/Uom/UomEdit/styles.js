import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Container: {
        padding: 0,
        marginLeft: 0,
    },
    Title: {
        ...theme.typography.title,
    },
    Wrapper: {
        padding: theme.spacing(1, 2),
    },
    TextWrapper: {
        padding: theme.spacing(2),
        backgroundColor: theme.palette.grey[200],
        marginTop: theme.spacing(1),
    },
}))
