import React, { useState, useEffect, useContext } from 'react'
import { Redirect, useParams } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
// context
import { AppContext } from '../../../contexts/app'
// hook
import useSocket from '../../../hooks/useSocket'
// @material-ui
import { Paper, Box, Container, Grid } from '@material-ui/core'
import { Select, MenuItem, InputLabel } from '@material-ui/core'
import { Typography, TextField } from '@material-ui/core'
import { FormControl, FormHelperText } from '@material-ui/core'
// components
import FormHeader from '../../../components/FormHeader'
import FormFooter from '../../../components/FormFooter'
// styles
import useStyles from './styles'

import initialItem from './initialItem'

const baseUrl = '/uoms'

function UomEdit() {
    const classes = useStyles()
    const [{ socket }] = useSocket()

    /** form data */
    const [data, setData] = useState(initialItem)

    /** fetch data from context */
    const [{ uoms }] = useContext(AppContext)

    /** fetch item by '_id' */
    const { _id } = useParams()
    useEffect(() => {
        if (!_id || isEmpty(uoms)) {
            return
        }

        const item = uoms.find((i) => i._id === _id)
        setData(item)
    }, [_id, uoms])

    /** form handlers */
    const handleInput = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        })
    }

    /** save/update item */
    const [isSaved, setIsSaved] = useState(false)
    const [errors, setErrors] = useState({})

    /** submit handler */
    const handleSubmit = () => {
        socket.emit('save', { model: 'Uom', eventName: 'uoms', data }, (res) =>
            res.errors ? setErrors(res.errors) : setIsSaved(true)
        )
    }

    if (isSaved) {
        return <Redirect to={baseUrl} />
    }

    return (
        <Container maxWidth='sm' classes={{ root: classes.Container }}>
            <Paper variant='outlined'>
                <FormHeader title={data._id ? 'UOM' : 'new UOM'} />
                <Box className={classes.Wrapper}>
                    <Grid container alignItems='center' spacing={2}>
                        <Grid item xs={8}>
                            <TextField
                                autoFocus
                                value={data.title}
                                onChange={handleInput}
                                name='title'
                                margin='normal'
                                id='title'
                                label='Название'
                                type='text'
                                fullWidth
                                required
                                error={Boolean(errors.title)}
                                helperText={errors.title}
                                InputProps={{
                                    classes: { input: classes.Title },
                                }}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                value={data.abbr}
                                onChange={handleInput}
                                name='abbr'
                                margin='normal'
                                id='abbr'
                                label='Аббревиатура'
                                type='text'
                                fullWidth
                                required
                                error={Boolean(errors.abbr)}
                                helperText={errors.abbr}
                                InputProps={{
                                    classes: { input: classes.Title },
                                }}
                            />
                        </Grid>
                    </Grid>

                    <Box className={classes.TextWrapper}>
                        <Grid container alignItems='center' spacing={2}>
                            <Grid item xs={1}>
                                <Typography variant='h2'>=</Typography>
                            </Grid>
                            <Grid item>
                                <TextField
                                    value={data.amount}
                                    onChange={handleInput}
                                    name='amount'
                                    id='amount'
                                    label='Количество'
                                    type='number'
                                    margin='none'
                                />
                            </Grid>

                            <Grid item xs>
                                <FormControl fullWidth error={!isEmpty(errors.uom)}>
                                    <InputLabel id='uoms' required>
                                        UOM
                                    </InputLabel>
                                    <Select
                                        labelId='uoms'
                                        id='uoms-select'
                                        name='ref'
                                        value={data.ref}
                                        onChange={handleInput}
                                    >
                                        {uoms.map(({ _id, abbr }) => (
                                            <MenuItem key={_id} value={_id}>
                                                {abbr}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                    {errors.ref && <FormHelperText>{errors.ref}</FormHelperText>}
                                </FormControl>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
                <FormFooter {...{ baseUrl, handleSubmit }} />
            </Paper>
        </Container>
    )
}

const isEqual = () => true

export default React.memo(UomEdit, isEqual)
