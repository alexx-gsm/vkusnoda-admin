import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom'
import { format } from 'date-fns'
import { ru } from 'date-fns/locale'
// context
import { AppContext } from '../../../contexts/app'
// material-table
import MaterialTable, { MTableToolbar } from 'material-table'
// material-ui
import { Container, Typography } from '@material-ui/core'
// components
import TableTitle from '../../../components/TableTitle'
// styles
import useStyles from './styles'

const baseUrl = '/outputs'

function OutputList() {
    const classes = useStyles()
    const history = useHistory()

    /** fetch data from context */
    const [appContext] = useContext(AppContext)
    const { outputs } = appContext

    const handleRowClick = (e, rowData) => history.push(`${baseUrl}/edit/${rowData._id}`)

    return (
        <Container maxWidth='sm' classes={{ root: classes.Container }}>
            <MaterialTable
                title={TableTitle('Выпуски')}
                columns={[
                    {
                        title: 'Дата',
                        field: 'date',
                        render: (rawData) => (
                            <Typography variant='h6'>
                                {format(new Date(rawData.date), 'd MMMM yyyy', { locale: ru })}
                            </Typography>
                        ),
                    },
                    {
                        title: 'Комментарии',
                        field: 'comment',
                    },
                ]}
                data={outputs}
                onRowClick={handleRowClick}
                options={{
                    sorting: true,
                    pageSize: 10,
                    pageSizeOptions: [5, 10, 50],
                    headerStyle: {
                        background: 'whitesmoke',
                    },
                }}
                actions={[
                    {
                        icon: 'add_circle',
                        tooltip: 'Add new outputs',
                        isFreeAction: true,
                        iconProps: {
                            classes: { root: classes.AddButton },
                            fontSize: 'large',
                        },
                        onClick: () => history.push(`${baseUrl}/edit/`),
                    },
                ]}
                components={{
                    Toolbar: (props) => (
                        <MTableToolbar {...props} classes={{ root: classes.Toolbar }} />
                    ),
                }}
            />
        </Container>
    )
}

export default OutputList
