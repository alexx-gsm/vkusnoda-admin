import React, { useState } from 'react'
// @material-ui
import { Grid } from '@material-ui/core'
import { Typography, TextField } from '@material-ui/core'
import { IconButton, MenuItem, Menu } from '@material-ui/core'
import { ListItemIcon } from '@material-ui/core'
// @material-icons
import CloseIcon from '@material-ui/icons/Close'
import MoreVertIcon from '@material-ui/icons/MoreVert'
// styles
import useStyles from './styles'

function DishRow({ dish, errors, handleRowInput, handleRowDelete }) {
    const classes = useStyles()
    const { _id, title, doneAmount = 0, outlookAmount = 0 } = dish

    const [anchorEl, setAnchorEl] = useState(null)
    const open = Boolean(anchorEl)

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }

    return (
        <Grid container wrap='nowrap' spacing={2} alignItems='center'>
            <Grid item xs>
                <Typography
                    variant='subtitle1'
                    noWrap
                    classes={{
                        root: classes.TextRowTitle,
                    }}
                >
                    {title}
                </Typography>
            </Grid>
            <Grid item>
                <TextField
                    value={doneAmount}
                    onChange={handleRowInput(_id)}
                    name='doneAmount'
                    id='doneAmount'
                    type='number'
                    margin='none'
                    required
                    error={Boolean(errors.doneAmount)}
                    helperText={errors.doneAmount}
                    InputProps={{
                        classes: {
                            input: classes.TextInput,
                        },
                    }}
                    classes={{
                        root: classes.TextNumber,
                    }}
                />
            </Grid>
            <Grid item>
                <TextField
                    value={outlookAmount}
                    onChange={handleRowInput(_id)}
                    name='outlookAmount'
                    id='outlookAmount'
                    type='number'
                    margin='none'
                    required
                    error={Boolean(errors.outlookAmount)}
                    helperText={errors.outlookAmount}
                    InputProps={{
                        classes: {
                            input: classes.TextInput,
                        },
                    }}
                    classes={{
                        root: classes.TextNumber,
                    }}
                />
            </Grid>

            <Grid item>
                <IconButton
                    aria-label='more'
                    aria-controls='more-actions'
                    aria-haspopup='true'
                    onClick={handleClick}
                    size='small'
                >
                    <MoreVertIcon />
                </IconButton>
                <Menu
                    id='more-actions'
                    anchorEl={anchorEl}
                    keepMounted
                    open={open}
                    onClose={handleClose}
                    PaperProps={{
                        style: {
                            width: 200,
                        },
                    }}
                >
                    <MenuItem onClick={handleRowDelete(_id)}>
                        <ListItemIcon>
                            <CloseIcon size='small' color='error' aria-label='delete' />
                        </ListItemIcon>
                        <Typography color='error'>Удалить</Typography>
                    </MenuItem>
                </Menu>
            </Grid>
        </Grid>
    )
}

export default DishRow
