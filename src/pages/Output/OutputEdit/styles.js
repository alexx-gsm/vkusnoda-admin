import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Container: {
        padding: 0,
        marginLeft: 0,
    },
    Paper: {
        paddingBottom: theme.spacing(1),
        margin: theme.spacing(3, 2, 0),
        backgroundColor: theme.palette.grey[50],
    },
    Section: {
        marginBottom: theme.spacing(2),
    },
    Wrapper: {
        padding: theme.spacing(1, 2),
    },
}))
