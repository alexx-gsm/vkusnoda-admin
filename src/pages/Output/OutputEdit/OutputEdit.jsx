import React, { useState, useEffect, useContext } from 'react'
import { Redirect, useParams } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
import { format } from 'date-fns'
import { ru } from 'date-fns/locale'
import sortDishCategories from '../../../helpers/sortDishCategories'
// context
import { AppContext } from '../../../contexts/app'
// hook
import useSocket from '../../../hooks/useSocket'
// @material-ui
import { Paper, Box, Container, Grid } from '@material-ui/core'
import { Typography, TextField } from '@material-ui/core'
// @material-lab
import Autocomplete from '@material-ui/lab/Autocomplete'
// components
import FormHeader from '../../../components/FormHeader'
import FormFooter from '../../../components/FormFooter'
import DateSelect from '../../../components/DateSelect'
import DishRow from './DishRow/DishRow'
// styles
import useStyles from './styles'

import initialItem from './initialItem'

const baseUrl = '/outputs'

function OutputEdit() {
    const classes = useStyles()
    const [{ socket }] = useSocket()

    /** form data */
    const [data, setData] = useState(initialItem)

    /** fetch data from context */
    const [{ outputs, dishes, menus, categories }] = useContext(AppContext)

    /** fetch item by '_id' */
    const { _id } = useParams()
    useEffect(() => {
        if (!_id || isEmpty(outputs)) {
            return
        }

        const item = outputs.find((i) => i._id === _id)
        setData(item)
    }, [_id, outputs])

    /** populate autocomplete's options */
    const [_menus, setMenus] = useState({})
    useEffect(() => {
        if (isEmpty(menus)) {
            return
        }
        setMenus({
            options: menus,
            getOptionLabel: (option) =>
                format(new Date(option.date), 'd MMMM yyyy', { locale: ru }),
        })
    }, [menus, setMenus])

    /** form handlers */
    const handleInput = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        })
    }

    const handleDateChange = (date) => {
        setData({
            ...data,
            date,
        })
    }

    const getDishes = (menuId) => {
        const menu = menus.find((item) => item._id === menuId)
        return menu ? menu.dishes : []
    }

    const handleAutocomplete = (e, val) => {
        setData({
            ...data,
            menuId: val ? val._id : null,
            dishes: val ? getDishes(val._id) : [],
        })
    }

    /** save/update item */
    const [isSaved, setIsSaved] = useState(false)
    const [errors, setErrors] = useState({})

    /** submit handler */
    const handleSubmit = () => {
        socket.emit('save', { model: 'Output', eventName: 'outputs', data }, (res) =>
            res.errors ? setErrors(res.errors) : setIsSaved(true)
        )
    }

    if (isSaved) {
        return <Redirect to={baseUrl} />
    }

    const getOption = (id) => {
        if (isEmpty(_menus)) {
            return null
        }

        const item = _menus.options.find((item) => item._id === id)
        return item ? item : null
    }

    const handleRowInput = (id) => (e) => {
        setData({
            ...data,
            dishes: data.dishes.map((dish) => {
                return dish._id === id
                    ? {
                          ...dish,
                          [e.target.name]: e.target.value,
                      }
                    : dish
            }),
        })
    }

    const handleRowDelete = (_id) => () => {
        setData({
            ...data,
            dishes: data.dishes.filter(({ dishId }) => dishId !== _id),
        })
    }

    return (
        <Container maxWidth='md' classes={{ root: classes.Container }}>
            <Paper variant='outlined'>
                <FormHeader title={data._id ? 'Выпуск' : 'Новый выпуск'} />
                <Box className={classes.Wrapper}>
                    <Grid container alignItems='flex-end' spacing={2}>
                        <Grid item>
                            <DateSelect date={data.date} handleDateChange={handleDateChange} />
                        </Grid>
                        <Grid item xs={4}>
                            {!isEmpty(_menus) && (
                                <Autocomplete
                                    {..._menus}
                                    id='menu'
                                    autoComplete
                                    value={getOption(data.menuId)}
                                    onChange={handleAutocomplete}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label='Меню'
                                            margin='none'
                                            fullWidth
                                            error={Boolean(errors.parent)}
                                            helperText={errors.parent}
                                        />
                                    )}
                                />
                            )}
                        </Grid>
                    </Grid>
                </Box>

                <Paper classes={{ root: classes.Paper }} variant='outlined' square>
                    {!isEmpty(data.dishes) && !isEmpty(dishes) && !isEmpty(categories) && (
                        <Box className={classes.Wrapper}>
                            {sortDishCategories(categories).map((category) => {
                                const categoriedDishes = data.dishes.filter(
                                    ({ categoryId }) => categoryId === category._id
                                )

                                return (
                                    !isEmpty(categoriedDishes) && (
                                        <Box key={category._id} classes={{ root: classes.Section }}>
                                            <Typography align='center' color='primary' variant='h5'>
                                                {category.title}
                                            </Typography>
                                            {categoriedDishes.map((dish, index) => (
                                                <DishRow
                                                    key={dish._id}
                                                    dish={dish}
                                                    errors={errors}
                                                    handleRowInput={handleRowInput}
                                                    handleRowDelete={handleRowDelete}
                                                />
                                            ))}
                                        </Box>
                                    )
                                )
                            })}
                        </Box>
                    )}
                </Paper>

                <Box className={classes.Wrapper}>
                    <TextField
                        value={data.comment}
                        onChange={handleInput}
                        name='comment'
                        margin='normal'
                        id='comment'
                        label='Комментарий'
                        type='text'
                        fullWidth
                        multiline
                        variant='outlined'
                        error={Boolean(errors.comment)}
                        helperText={errors.comment}
                    />
                </Box>

                <FormFooter {...{ baseUrl, handleSubmit }} />
            </Paper>
        </Container>
    )
}

export default OutputEdit
