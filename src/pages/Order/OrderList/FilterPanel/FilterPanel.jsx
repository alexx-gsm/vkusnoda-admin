import React, { useCallback } from 'react'
// 2material-ui
import { Paper, Grid } from '@material-ui/core'
import { FormControl, FormGroup, FormControlLabel } from '@material-ui/core'
import { Select, Checkbox, MenuItem, InputLabel } from '@material-ui/core'
// components
import DateSelect from '../../../../components/DateSelect'
// styles
import useStyles from './styles'

function FilterPanel({ couriers, filter, setFilter }) {
    const classes = useStyles()

    const handleSelect = useCallback(
        (e) => {
            setFilter({
                ...filter,
                [e.target.name]: e.target.value,
            })
        },
        [filter, setFilter]
    )

    const handleCheckbox = useCallback(
        (e) => {
            setFilter({
                ...filter,
                [e.target.name]: e.target.checked,
            })
        },
        [filter, setFilter]
    )

    const handleDateChange = useCallback(
        (date) => {
            setFilter({
                ...filter,
                date,
            })
        },
        [filter, setFilter]
    )

    return (
        <Paper square variant='outlined' classes={{ root: classes.Paper }}>
            <Grid container spacing={2} alignItems='center'>
                <Grid item>
                    <DateSelect date={filter.date} handleDateChange={handleDateChange} />
                </Grid>
                <Grid item>
                    <FormControl variant='outlined' style={{ minWidth: 150 }}>
                        <InputLabel id='status-filter'>Статус</InputLabel>
                        <Select
                            labelId='status-filter'
                            id='status-select'
                            name='status'
                            value={filter.status}
                            onChange={handleSelect}
                            label='Статус'
                        >
                            <MenuItem value=''>
                                <em>-</em>
                            </MenuItem>
                            <MenuItem value='Черновик'>Черновик</MenuItem>
                            <MenuItem
                                value='Новый'
                                classes={{
                                    root: classes.StatusNew,
                                    selected: classes.StatusNew1,
                                }}
                            >
                                Новый
                            </MenuItem>
                            <MenuItem value='Сборка'>Сборка</MenuItem>
                            <MenuItem value='Готов'>Готов</MenuItem>
                            <MenuItem value='Доставка'>Доставка</MenuItem>
                            <MenuItem value='Доставлен'>Доставлен</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item>
                    <FormControl variant='outlined' style={{ minWidth: 130 }}>
                        <InputLabel id='courier-filter'>Курьер</InputLabel>
                        <Select
                            labelId='courier-filter'
                            id='couriers-select'
                            name='courier'
                            value={filter.courier}
                            onChange={handleSelect}
                            label='Курьер'
                        >
                            <MenuItem value=''>
                                <em>-</em>
                            </MenuItem>
                            {couriers.map(({ _id, title }) => (
                                <MenuItem key={_id} value={_id}>
                                    {title}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item>
                    <FormControl variant='outlined' style={{ minWidth: 130 }}>
                        <InputLabel id='payment-filter'>Оплата</InputLabel>
                        <Select
                            labelId='payment-filter'
                            id='payment-select'
                            name='payment'
                            value={filter.payment}
                            onChange={handleSelect}
                            label='Оплата'
                        >
                            <MenuItem value=''>
                                <em>-</em>
                            </MenuItem>
                            <MenuItem value='cash'>Наличные</MenuItem>
                            <MenuItem value='card'>Карта</MenuItem>
                            <MenuItem value='account'>Безнал</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item>
                    <FormGroup row>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={filter.isPaid}
                                    onChange={handleCheckbox}
                                    name='isPaid'
                                />
                            }
                            label='Неоплаченные'
                        />
                    </FormGroup>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default FilterPanel
