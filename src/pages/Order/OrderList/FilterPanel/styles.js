import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Paper: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(1),
    },
    StatusNew: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        '&:hover': {
            backgroundColor: theme.palette.primary.dark,
        },
        '&$StatusNew1': {
            backgroundColor: theme.palette.primary.main,
        },
        '&$StatusNew1:hover': {
            backgroundColor: theme.palette.primary.dark,
        },
    },
    StatusNew1: {},
}))
