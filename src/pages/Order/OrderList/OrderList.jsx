import React, { Fragment, useState, useEffect, useContext, useCallback } from 'react'
import { useHistory } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
import { format } from 'date-fns'
import { ru } from 'date-fns/locale'
import formatter from '../../../helpers/formatter'
import { getDayInterval } from '../../../helpers/queryTemplates'
// context
import { AppContext, SET_ITEMS } from '../../../contexts/app'
// hooks
import useSocket from '../../../hooks/useSocket'
// @material-ui
import { Box, Typography } from '@material-ui/core'
import { Table, TableHead, TableBody, TableRow, TableCell } from '@material-ui/core'
import { IconButton } from '@material-ui/core'
import { Checkbox, Chip, Fade } from '@material-ui/core'
import { Menu, MenuItem } from '@material-ui/core'
// material-icon
import CreditCardIcon from '@material-ui/icons/CreditCard'
import EuroIcon from '@material-ui/icons/Euro'
import WorkIcon from '@material-ui/icons/Work'
import MoreVertIcon from '@material-ui/icons/MoreVert'
// components
import Header from '../../../components/common/Header'
import FilterPanel from './FilterPanel'
// styles
import useStyles from './styles'
// settings
import { baseUrl, initialFilter } from './settings'
import orderModel from '../../../models/order'

function OrderList() {
    const classes = useStyles()
    const history = useHistory()
    const [{ socket }] = useSocket()

    const { model, eventName } = orderModel

    const [{ orders, clients, couriers }, dispatch] = useContext(AppContext)

    const [filter, setFilter] = useState(initialFilter)

    useEffect(() => {
        socket.emit(
            `getItemsOf${model}`,
            {
                date: getDayInterval(new Date(filter.date)),
            },
            (items) => {
                dispatch({
                    type: SET_ITEMS,
                    payload: { [eventName]: items },
                })
            }
        )
    }, [filter.date, model, eventName, dispatch, socket])

    const [filteredOrders, setFilteredOrders] = useState(orders)

    const getFilteredOrders = useCallback(() => {
        const { date, ..._filters } = filter

        return Object.keys(_filters).reduce((acc, f) => {
            return _filters[f] ? acc.filter((order) => order[f] === _filters[f]) : acc
        }, orders)
    }, [filter, orders])

    useEffect(() => {
        const _orders = getFilteredOrders()
        setFilteredOrders(_orders)
    }, [filter, setFilteredOrders, getFilteredOrders])

    const [searchField, setSearchField] = useState('')

    const [isCheckedRows, setCheckedRows] = useState([])
    const handleCheckedRow = useCallback(
        (row) => (e) => {
            e.target.checked
                ? setCheckedRows([...isCheckedRows, row])
                : setCheckedRows(isCheckedRows.filter((r) => r !== row))
        },
        [isCheckedRows, setCheckedRows]
    )

    /** row dropdown menu */
    const [anchorEl, setAnchorEl] = useState({})
    const handleClickMenuButton = useCallback(
        (_id) => (event) => {
            setAnchorEl({
                ...anchorEl,
                [_id]: event.currentTarget,
            })
        },
        [anchorEl, setAnchorEl]
    )

    const handleClose = useCallback(
        () => () => {
            setAnchorEl(null)
        },
        [setAnchorEl]
    )

    const handleClickMenuItem = useCallback(
        (_id) => () => {
            history.push(`${baseUrl}/edit/${_id}`)
        },
        [history]
    )

    // console.log('filter', filter)

    return (
        <Fragment>
            <Header
                title='Заказы'
                filter={searchField}
                setFilter={setSearchField}
                disabled={true}
                handleClick={() => history.push(`${baseUrl}/edit/`)}
            />

            <FilterPanel couriers={couriers} filter={filter} setFilter={setFilter} />

            <Table size='small'>
                <TableHead className={classes.TableHead}>
                    <TableRow>
                        <TableCell padding='checkbox'></TableCell>
                        <TableCell align='center'>Статус</TableCell>
                        <TableCell>Дата</TableCell>
                        <TableCell>Номер</TableCell>
                        <TableCell style={{ width: '100%' }}>Клиент</TableCell>
                        <TableCell>Адрес</TableCell>
                        <TableCell>Курьер</TableCell>
                        <TableCell>Оплата</TableCell>
                        <TableCell>Сумма</TableCell>
                        <TableCell padding='checkbox'></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {filteredOrders.map((order) => {
                        const checked = isCheckedRows.includes(order._id)
                        return (
                            <TableRow key={order._id} selected={checked}>
                                <TableCell padding='none'>
                                    <Checkbox
                                        checked={checked}
                                        onChange={handleCheckedRow(order._id)}
                                        color='primary'
                                        classes={{ root: classes.Checkbox }}
                                    />
                                </TableCell>
                                <TableCell>{getStatus(order.status)}</TableCell>
                                <TableCell>{getDate(order.date)}</TableCell>
                                <TableCell>{getNumber(order.number)}</TableCell>
                                <TableCell>{getClientName(order.client)}</TableCell>
                                <TableCell>{getAddress(order.address)}</TableCell>
                                <TableCell>{getCourier(order.courier)}</TableCell>
                                <TableCell align='center'>{getPayment(order.payment)}</TableCell>
                                <TableCell>{getOrderTotalPrice(order)}</TableCell>
                                <TableCell padding='none'>{getMenu(order)}</TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
        </Fragment>
    )

    function getStatus(status) {
        return (
            <Chip
                label={status}
                color={getStatusColor(status)}
                classes={{ root: classes.ChipStatus }}
            />
        )
    }

    function getDate(date) {
        return (
            <Typography variant='subtitle2' noWrap>
                {format(new Date(date), 'd MMMM', { locale: ru })}
            </Typography>
        )
    }

    function getNumber(number) {
        return (
            <Typography variant='subtitle2' noWrap>
                {number}
            </Typography>
        )
    }

    function getClientName(id) {
        if (isEmpty(clients)) {
            return ''
        }

        const client = clients.find((c) => c._id === id)

        return client ? (
            <Fragment>
                <Typography variant='h6'>{client.organization}</Typography>
                <Typography variant='caption'>{client.name}</Typography>
            </Fragment>
        ) : (
            ''
        )
    }

    function getAddress(address) {
        return (
            <Typography variant='subtitle2' noWrap>
                {address}
            </Typography>
        )
    }

    function getCourier(courierId) {
        if (isEmpty(couriers)) {
            return ''
        }
        const courier = couriers.find((c) => c._id === courierId)
        return courier ? courier.title : ''
    }

    function getPayment(payment) {
        switch (payment) {
            case 'card':
                return <CreditCardIcon />
            case 'cash':
                return <EuroIcon />
            default:
                return <WorkIcon />
        }
    }

    function getMenu(order) {
        return (
            <Box>
                <IconButton
                    aria-label='more'
                    aaria-controls={`order-${order._id}`}
                    aria-haspopup='true'
                    size='small'
                    disableRipple
                    edge='end'
                    onClick={handleClickMenuButton(order._id)}
                >
                    <MoreVertIcon />
                </IconButton>
                <Menu
                    id={`order-${order._id}`}
                    anchorEl={anchorEl ? anchorEl[order._id] : null}
                    keepMounted
                    open={Boolean(anchorEl && anchorEl[order._id])}
                    onClose={handleClose(order._id)}
                    TransitionComponent={Fade}
                >
                    <MenuItem onClick={handleClickMenuItem(order._id)}>Редактировать</MenuItem>
                    <MenuItem onClick={handleClickMenuItem(order._id)} disabled>
                        Печать
                    </MenuItem>
                </Menu>
            </Box>
        )
    }

    function getStatusColor(status) {
        switch (status) {
            case 'Новый':
                return 'primary'
            default:
                return 'default'
        }
    }

    function getOrderTotalPrice(order) {
        if (!order || isEmpty(order.dishes)) {
            return '-'
        }

        const total = order.dishes.reduce((acc, dish) => {
            const x = isEmpty(dish.complexDish)
                ? Number(dish.price) * Number(dish.amount)
                : Number(dish.amount) * (+dish.price + +dish.complexDish.price)
            return acc + x
        }, 0)

        return formatter.format(total)
    }
}

export default OrderList
