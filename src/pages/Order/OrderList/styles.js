import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    TableHead: {
        '& th': {
            textTransform: 'uppercase',
            color: theme.palette.grey[600],
            fontWeight: theme.typography.fontWeightLight,
        },
    },
    Toolbar: {
        backgroundColor: theme.palette.grey[100],
        paddingLeft: theme.spacing(2),
    },
    AddButton: {
        color: theme.palette.secondary.dark,
    },
    Checkbox: {
        padding: 0,
    },
    ChipStatus: {
        textTransform: 'uppercase',
    },
}))
