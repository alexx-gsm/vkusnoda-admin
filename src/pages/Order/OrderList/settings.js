export const baseUrl = '/orders'

export const initialFilter = {
    date: new Date(),
    status: '',
    courier: '',
    payment: '',
    isPaid: false,
}
