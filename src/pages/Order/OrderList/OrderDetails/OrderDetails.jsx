import React, { useState, useEffect } from 'react'
import { Paper, Typography, TableFooter } from '@material-ui/core'
import isEmpty from '../../../../helpers/is-empty'
import formatter from '../../../../helpers/formatter'
// @material-ui
import { Table, TableHead, TableBody, TableRow, TableCell } from '@material-ui/core'
// styles
import useStyles from './styles'

function OrderDetails({ order }) {
    const classes = useStyles()

    const [prices, setPrices] = useState({})
    useEffect(() => {
        if (!order || isEmpty(order.dishes)) {
            return
        }

        let total = 0
        const prices = order.dishes.reduce((acc, dish, index) => {
            const rowTotal = isEmpty(dish.complexDish)
                ? Number(dish.price) * Number(dish.amount)
                : Number(dish.amount) * (+dish.price + +dish.complexDish.price)
            total += rowTotal

            return {
                ...acc,
                [index]: rowTotal,
                total,
            }
        }, {})

        setPrices(prices)
    }, [order, setPrices])

    return (
        <Paper classes={{ root: classes.Paper }}>
            {!isEmpty(order.dishes) ? (
                <Table>
                    {getTableHead()}
                    {getTableBody()}
                    {getTableFooter()}
                </Table>
            ) : (
                ''
            )}
        </Paper>
    )

    function getTableHead() {
        return (
            <TableHead>
                <TableRow>
                    <TableCell padding='checkbox' style={{ width: '100%' }} align='right'>
                        Название
                    </TableCell>
                    <TableCell>Цена</TableCell>
                    <TableCell>Количество</TableCell>
                    <TableCell>Итого</TableCell>
                </TableRow>
            </TableHead>
        )
    }

    function getTableBody() {
        return (
            <TableBody>
                {order.dishes.map((dish, index) => {
                    return (
                        <TableRow key={index}>
                            <TableCell padding='checkbox'>
                                <Typography
                                    variant='subtitle2'
                                    noWrap
                                    color='primary'
                                    align='right'
                                >
                                    {dish.title}
                                </Typography>
                                {!isEmpty(dish.complexDish) && (
                                    <Typography variant='subtitle2' color='secondary' align='right'>
                                        {dish.complexDish.title}
                                    </Typography>
                                )}
                            </TableCell>
                            <TableCell align='center'>
                                <Typography variant='subtitle2'>
                                    {formatter.format(dish.price)}
                                </Typography>
                                {!isEmpty(dish.complexDish) && (
                                    <Typography variant='subtitle2'>
                                        {formatter.format(dish.complexDish.price)}
                                    </Typography>
                                )}
                            </TableCell>
                            <TableCell align='center'>{`${dish.amount} шт.`}</TableCell>
                            <TableCell>
                                <Typography variant='h6' align='right'>
                                    {formatter.format(prices[index])}
                                </Typography>
                            </TableCell>
                        </TableRow>
                    )
                })}
            </TableBody>
        )
    }

    function getTableFooter() {
        return (
            <TableFooter className={classes.Footer}>
                <TableRow>
                    <TableCell colSpan={2}></TableCell>
                    <TableCell colSpan={2} padding='none'>
                        <Typography align='right' variant='h2' classes={{ root: classes.Total }}>
                            {formatter.format(prices.total)}
                        </Typography>
                    </TableCell>
                </TableRow>
            </TableFooter>
        )
    }
}

export default OrderDetails
