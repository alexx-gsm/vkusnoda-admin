import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Paper: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
        backgroundColor: theme.palette.grey[200],
    },
    RowHeader: {
        backgroundColor: theme.palette.primary.main,
    },
    RowWrapper: {
        padding: theme.spacing(1),
    },
    Row: {
        borderBottom: `1px dotted ${theme.palette.grey[400]}`,
    },
    TitleHeader: {
        textTransform: 'uppercase',
        color: theme.palette.primary.contrastText,
    },
    Title: {
        color: theme.palette.primary.dark,
    },
    Total: {
        borderTop: `4px solid ${theme.palette.grey[700]}`,
        color: theme.palette.grey[700],
        fontWeight: theme.typography.fontWeightMedium,
    },
    Footer: {
        '& td': {
            borderBottom: 'none',
        },
    },
}))
