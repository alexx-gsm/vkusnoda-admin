import React, { useState, useEffect, useContext } from 'react'
import isEmpty from '../../../../helpers/is-empty'
import { compareAsc, startOfDay } from 'date-fns'
import formatter from '../../../../helpers/formatter'
// context
import { AppContext } from '../../../../contexts/app'
// @material-ui
import { Grid, Typography, TableFooter, TableRow, TableCell } from '@material-ui/core'
import { TableContainer, Table, TableBody } from '@material-ui/core'
// components
import RowDish from '../OrderedDish'
import MenuPanel from '../MenuPanel/MenuPanel'
// styles
import useStyles from './styles'

function OrderedDishes({ date, orderedDishes, setOrderedDishes }) {
    const classes = useStyles()

    /** fetch data from context */
    const [{ menus, categories }] = useContext(AppContext)

    /** total price */
    const [total, setTotal] = useState(0)

    useEffect(() => {
        if (!orderedDishes) {
            setTotal(0)
            return
        }

        const total = orderedDishes.reduce((acc, od) => {
            return od.isComplex && !isEmpty(od.complexDish)
                ? acc + od.amount * (+od.price + +od.complexDish.price)
                : acc + od.amount * od.price
        }, 0)
        setTotal(total)
    }, [orderedDishes, setTotal])

    /** menu dishes */
    const [_dishes, setDishes] = useState([])

    useEffect(() => {
        if (isEmpty(menus)) {
            return
        }

        const menu = menus.find(
            (m) => compareAsc(startOfDay(date), startOfDay(new Date(m.date))) === 0
        )

        menu ? setDishes(menu.dishes) : setDishes([])
    }, [date, menus, setDishes])

    /** handlers */
    const handleAdd = (orderedDish) => () => {
        setOrderedDishes([
            ...orderedDishes,
            {
                ...orderedDish,
                amount: 1,
                ...(orderedDish.isComplex
                    ? {
                          complexDish: {},
                      }
                    : null),
            },
        ])
    }

    const handleDelete = (index) => () => {
        setOrderedDishes([...orderedDishes.slice(0, index), ...orderedDishes.slice(index + 1)])
    }

    const handleChange = (index) => (sign) => () => {
        setOrderedDishes([
            ...orderedDishes.slice(0, index),
            {
                ...orderedDishes[index],
                amount:
                    sign === '+' ? ++orderedDishes[index].amount : --orderedDishes[index].amount,
            },
            ...orderedDishes.slice(index + 1),
        ])
    }

    const handleAddGarnish = (index) => (complexDish) => {
        setOrderedDishes([
            ...orderedDishes.slice(0, index),
            {
                ...orderedDishes[index],
                complexDish,
            },
            ...orderedDishes.slice(index + 1),
        ])
    }

    return (
        <Grid container>
            <Grid item xs={isEmpty(_dishes) ? 12 : 9}>
                <TableContainer classes={{ root: classes.TableContainer }}>
                    <Table className={classes.table} size='small' aria-label='ordered dishes'>
                        <TableBody>
                            {!isEmpty(orderedDishes) &&
                                orderedDishes.map((row, index) => {
                                    return (
                                        <RowDish
                                            key={index}
                                            dish={row}
                                            menuDishes={_dishes}
                                            categories={categories}
                                            onChangeAmount={handleChange(index)}
                                            onAddGarnish={handleAddGarnish(index)}
                                            onDelete={handleDelete(index)}
                                        />
                                    )
                                })}
                        </TableBody>
                        {!isEmpty(orderedDishes) && (
                            <TableFooter className={classes.Footer}>
                                <TableRow>
                                    <TableCell colSpan={3}>
                                        <Typography variant='h4' align='right'>
                                            итого:
                                        </Typography>
                                    </TableCell>
                                    <TableCell colSpan={2}>
                                        <Typography
                                            variant='h3'
                                            align='right'
                                            classes={{ root: classes.Total }}
                                        >
                                            {formatter.format(total)}
                                        </Typography>
                                    </TableCell>
                                </TableRow>
                            </TableFooter>
                        )}
                    </Table>
                </TableContainer>
            </Grid>
            {/** MENU PANEL */}
            {!isEmpty(_dishes) && !isEmpty(categories) && (
                <Grid item xs={3}>
                    <MenuPanel
                        date={date}
                        menuDishes={_dishes}
                        categories={categories}
                        handleAdd={handleAdd}
                    />
                </Grid>
            )}
        </Grid>
    )
}
export default OrderedDishes
