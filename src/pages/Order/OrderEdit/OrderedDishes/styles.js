import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    TypoAmount: {
        padding: theme.spacing(0, 1),
        minWidth: theme.spacing(8),
    },
    PaperDish: {
        padding: theme.spacing(0.5),
    },
    Footer: {
        '& td': {
            border: 'none'
        }
    },
    Total: {
        color: theme.palette.grey[800],
        fontWeight: theme.typography.fontWeightBold
    }
}))
