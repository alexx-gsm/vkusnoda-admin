export const MENUPANEL_TAB_1 = [
    {
        title: 'Суп',
        color: 'red',
    },
    {
        title: 'Салат',
        color: 'teal',
    },
]
export const MENUPANEL_TAB_2 = [
    {
        title: 'Горячее',
        color: 'amber',
    },
    {
        title: 'Гарнир',
        color: 'brown',
    },
]
export const MENUPANEL_TAB_3 = [
    {
        title: 'Выпечка',
        color: 'deepOrange',
    },
    {
        title: 'Соусы',
        color: 'cyan',
    },
    {
        title: 'Напитки',
        color: 'blueGrey',
    },
]
