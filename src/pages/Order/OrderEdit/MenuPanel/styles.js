import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Paper: {
        margin: theme.spacing(0.5),
        backgroundColor: theme.palette.grey[100]
    }
}))