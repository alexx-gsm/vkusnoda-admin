import React, { useState, useEffect, useContext, Fragment } from 'react'
import SwipeableViews from 'react-swipeable-views'
import { isEqual, startOfDay } from 'date-fns'
import isEmpty from '../../../../helpers/is-empty'
// context
import { AppContext } from '../../../../contexts/app'
// @material-ui
import { AppBar, Tabs, Paper, Typography } from '@material-ui/core'
// components
import MenuPanelTab from '../MenuPanelTab'
import MenuPanelItem from '../MenuPanelItem'
// consts
import { MENUPANEL_TAB_1, MENUPANEL_TAB_2, MENUPANEL_TAB_3 } from './tabs'
// styles
import useStyles from './styles'

function MenuPanel({ date, menuDishes, categories, handleAdd }) {
    const classes = useStyles()

    /** fetch data from context */
    const [{ outputs, orders }] = useContext(AppContext)

    // output dishes
    const [outputDishes, setOutputDishes] = useState([])
    useEffect(() => {
        if (isEmpty(outputs)) {
            return
        }

        const output = outputs.find((o) => isEqual(startOfDay(date), startOfDay(new Date(o.date))))
        setOutputDishes(output.dishes)
    }, [date, outputs, setOutputDishes])

    // orders by date
    const [datedOrders, setDatedOrders] = useState([])
    useEffect(() => {
        if (isEmpty(orders)) {
            return
        }

        const _orders = orders.filter((o) =>
            isEqual(startOfDay(date), startOfDay(new Date(o.date)))
        )
        setDatedOrders(_orders)
    }, [date, orders, setDatedOrders])

    // get count of ready to order dish
    const getCountToOrderDish = (dishId) => {
        let ordered = 0,
            done = 0,
            outlook = 0

        if (!isEmpty(datedOrders)) {
            datedOrders.map((order) =>
                order.dishes
                    .filter(
                        (od) => od._id === dishId || (od.isComplex && od.complexDish._id === dishId)
                    )
                    .map((od) => (ordered += od.amount))
            )
        }

        if (!isEmpty(outputDishes)) {
            const outputDish = outputDishes.find((od) => od._id === dishId)
            if (outputDish) {
                done = outputDish.doneAmount ? outputDish.doneAmount : 0
                outlook = outputDish.outlookAmount ? outputDish.outlookAmount : 0
            }
        }

        return { ordered, done, outlook }
    }

    const [value, setValue] = useState(0)

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    const handleChangeIndex = (index) => {
        setValue(index)
    }

    const getCategoriedDishes = (categoryTitles) => {
        console.log('categoryTitles', categoryTitles)
        return categoryTitles.reduce((acc, cat) => {
            const category = categories.find((c) => c.title === cat.title)
            console.log('category', category)
            return {
                ...acc,
                [cat.title]: menuDishes.filter((md) => md.categoryId === category._id),
            }
        }, {})
    }

    const getLabel = (label_1, label_2) => {
        return (
            <Fragment>
                <Typography variant='subtitle1'>{label_1}</Typography>
                <Typography variant='subtitle2'>{label_2}</Typography>
            </Fragment>
        )
    }

    return (
        <Paper square variant='outlined' classes={{ root: classes.Paper }}>
            <AppBar position='static' color='default' square>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor='primary'
                    textColor='primary'
                    variant='fullWidth'
                >
                    <MenuPanelTab label={getLabel('Супы', 'Салаты')} />
                    <MenuPanelTab label={getLabel('Горячее', 'Гарниры')} />
                    <MenuPanelTab label='Разное' />
                </Tabs>
            </AppBar>
            <SwipeableViews index={value} onChangeIndex={handleChangeIndex}>
                <MenuPanelItem
                    value={value}
                    index={0}
                    items={getCategoriedDishes(MENUPANEL_TAB_1)}
                    titles={MENUPANEL_TAB_1}
                    handleAdd={handleAdd}
                    handleCountDish={getCountToOrderDish}
                />
                <MenuPanelItem
                    value={value}
                    index={1}
                    items={getCategoriedDishes(MENUPANEL_TAB_2)}
                    titles={MENUPANEL_TAB_2}
                    handleAdd={handleAdd}
                    handleCountDish={getCountToOrderDish}
                />
                <MenuPanelItem
                    value={value}
                    index={2}
                    items={getCategoriedDishes(MENUPANEL_TAB_3)}
                    titles={MENUPANEL_TAB_3}
                    handleAdd={handleAdd}
                    handleCountDish={getCountToOrderDish}
                />
            </SwipeableViews>
        </Paper>
    )
}

export default MenuPanel
