import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    TypoAmount: {
        padding: theme.spacing(0, 1),
        minWidth: theme.spacing(8),
    },
}))
