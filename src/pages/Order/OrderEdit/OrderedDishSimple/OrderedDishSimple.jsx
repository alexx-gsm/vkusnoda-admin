import React from 'react'
import formatter from '../../../../helpers/formatter'
// @material-ui
import { Grid } from '@material-ui/core'
import { Typography, IconButton } from '@material-ui/core'
import { TableRow, TableCell } from '@material-ui/core'
// @material-icon
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import CloseIcon from '@material-ui/icons/Close'
// styles
import useStyles from './styles'

function OrderedDishSimple({ dish, onChangeAmount, onDelete }) {
    const classes = useStyles()

    return (
        <TableRow>
            <TableCell padding='none' size='small'>
                <IconButton onClick={onDelete} aria-label='delete' size='small' color='primary'>
                    <CloseIcon />
                </IconButton>
            </TableCell>
            <TableCell size='small' style={{ width: '100%' }}>
                <Typography variant='h6' noWrap>
                    {dish.title}
                </Typography>
            </TableCell>
            <TableCell align='center' size='small'>
                <Typography variant='caption'>{formatter.format(dish.price)}</Typography>
            </TableCell>
            <TableCell align='center' size='small' padding='none'>
                <Grid item container style={{ width: 'auto', minWidth: '132px' }}>
                    <IconButton
                        onClick={onChangeAmount('-')}
                        disabled={dish.amount <= 1}
                        aria-label='minus'
                        size='small'
                        color='secondary'
                    >
                        <ChevronLeftIcon />
                    </IconButton>
                    <Typography align='center' variant='h4' classes={{ root: classes.TypoAmount }}>
                        {dish.amount}
                    </Typography>
                    <IconButton
                        onClick={onChangeAmount('+')}
                        aria-label='plus'
                        size='small'
                        color='secondary'
                    >
                        <ChevronRightIcon />
                    </IconButton>
                </Grid>
            </TableCell>
            <TableCell size='small'>
                <Typography align='right' variant='h6'>
                    {formatter.format(dish.price * dish.amount)}
                </Typography>
            </TableCell>
        </TableRow>
    )
}

export default OrderedDishSimple
