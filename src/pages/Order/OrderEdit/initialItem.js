export default {
    date: new Date(),
    organization: '',
    phones: [],
    address: '',
    courier: '',
    payment: 'cash',
    discount: 0,
    status: 'Черновик',
    dishes: [],
    comment: '',
    notes: [],
}
