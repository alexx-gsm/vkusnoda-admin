import React, { useState } from 'react'
import formatter from '../../../../helpers/formatter'
// @material-ui
import { Grid } from '@material-ui/core'
import { Typography, Button, IconButton } from '@material-ui/core'
import { Menu, MenuItem } from '@material-ui/core'
import { TableRow, TableCell } from '@material-ui/core'
// @material-icon
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import CloseIcon from '@material-ui/icons/Close'
// styles
import useStyles from './styles'

function OrderedDishComplex({
    dish,
    menuDishes,
    categories,
    onChangeAmount,
    onAddGarnish,
    onDelete,
}) {
    const classes = useStyles()

    /** menu dropdown states */
    const [anchorEl, setAnchorEl] = useState({})
    /** menu dropdown handler */
    const handleClick = (_id) => (event) => {
        setAnchorEl({
            ...anchorEl,
            [_id]: event.currentTarget,
        })
    }
    const handleClose = (_id) => () => {
        setAnchorEl({
            ...anchorEl,
            [_id]: null,
        })

        setAnchorEl(null)
    }

    const handleMenu = (menuDish, dish) => (e) => {
        onAddGarnish(menuDish)
        handleClose(dish._id)()
    }

    const getGarnishes = () => {
        const garnishCategiry = categories.find((c) => c.title === 'Гарнир')

        return garnishCategiry
            ? menuDishes.filter((md) => md.categoryId === garnishCategiry._id)
            : []
    }

    return (
        <TableRow>
            <TableCell padding='none' size='small'>
                <IconButton onClick={onDelete} aria-label='delete' size='small' color='primary'>
                    <CloseIcon />
                </IconButton>
            </TableCell>
            <TableCell size='small' style={{ width: '100%' }}>
                <Typography variant='h6' noWrap>
                    {dish.title}
                </Typography>
                <Button
                    onClick={handleClick(dish._id)}
                    size='small'
                    aria-controls={`complex-${dish._id}`}
                    variant='text'
                    color='primary'
                >
                    + Гарнир
                </Button>
                <Menu
                    id={`complex-${dish._id}`}
                    anchorEl={anchorEl ? anchorEl[dish._id] : null}
                    keepMounted
                    open={Boolean(anchorEl && anchorEl[dish._id])}
                    onClose={handleClose(dish._id)}
                >
                    {getGarnishes().map((menuDish) => (
                        <MenuItem key={menuDish._id} onClick={handleMenu(menuDish, dish)}>
                            {menuDish.title}
                        </MenuItem>
                    ))}
                </Menu>
            </TableCell>
            <TableCell align='center' size='small'>
                <Typography variant='caption'>{formatter.format(dish.price)}</Typography>
            </TableCell>
            <TableCell align='center' size='small' padding='none'>
                <Grid item container style={{ width: 'auto', minWidth: '132px' }}>
                    <IconButton
                        onClick={onChangeAmount('-')}
                        disabled={dish.amount <= 1}
                        aria-label='minus'
                        size='small'
                        color='secondary'
                    >
                        <ChevronLeftIcon />
                    </IconButton>
                    <Typography align='center' variant='h4' classes={{ root: classes.TypoAmount }}>
                        {dish.amount}
                    </Typography>
                    <IconButton
                        onClick={onChangeAmount('+')}
                        aria-label='plus'
                        size='small'
                        color='secondary'
                    >
                        <ChevronRightIcon />
                    </IconButton>
                </Grid>
            </TableCell>
            <TableCell size='small'>
                <Typography align='right' variant='h6'>
                    {formatter.format(dish.price * dish.amount)}
                </Typography>
            </TableCell>
        </TableRow>
    )
}

export default OrderedDishComplex
