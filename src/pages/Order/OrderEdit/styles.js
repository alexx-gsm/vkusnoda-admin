import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Container: {
        padding: 0,
        marginLeft: 0,
    },
    WrapperHeader: {
        padding: theme.spacing(1, 2, 2),
        backgroundColor: theme.palette.grey[300],
    },
    WrapperDate: {
        paddingTop: theme.spacing(3.25),
    },
    TypographyMain: {
        '& input': {
            ...theme.typography.h6,
        },
    },
    TypographySecondary: {
        color: theme.palette.grey[800],
        fontWeight: theme.typography.fontWeightMedium,
        '& em': {
            paddingRight: theme.spacing(1),
            fontWeight: theme.typography.fontWeightLight,
        },
    },
    Status: {
        backgroundColor: theme.palette.secondary.dark,
        color: theme.palette.secondary.contrastText,
        textTransform: 'uppercase',
        marginTop: theme.spacing(2),
    },
    //
    Wrapper: {
        padding: theme.spacing(1, 2),
    },
    Title: {
        ...theme.typography.title,
    },
    RadioWrapper: {
        margin: theme.spacing(2, 0),
        padding: theme.spacing(2),
        backgroundColor: theme.palette.grey[200],
    },
    Footer: {
        padding: theme.spacing(2),
        display: 'flex',
        justifyContent: 'space-between',
    },
    Button: {
        marginLeft: 'auto',
        marginRight: theme.spacing(1),
    },
}))
