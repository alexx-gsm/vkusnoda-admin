import React from 'react'
import Tab from '@material-ui/core/Tab'
// styles
import useStyles from './styles'

function MenuPanelTab(props) {
    const classes = useStyles()

    return <Tab {...props} classes={{ root: classes.Tab }} />
}

export default MenuPanelTab
