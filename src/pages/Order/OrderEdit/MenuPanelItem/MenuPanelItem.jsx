import React from 'react'
import isEmpty from '../../../../helpers/is-empty'
// @material-ui
import { Paper, Grid } from '@material-ui/core'
import { Card, CardActionArea, CardContent } from '@material-ui/core'
import { Typography } from '@material-ui/core'
// components
import BadgeInfo from '../BadgeInfo'
// styles
import useStyles from './styles'

function MenuPanelItem({ items, titles, handleAdd, handleCountDish }) {
    const classes = useStyles()

    return titles.map(
        (titleObj) =>
            !isEmpty(items[titleObj.title]) && (
                <Paper
                    key={titleObj.title}
                    square
                    classes={{ root: classes.Paper }}
                    className={titleObj.color}
                >
                    <Grid container spacing={1}>
                        {items[titleObj.title].map((dish) => {
                            const { ordered, done, outlook } = handleCountDish(dish._id)
                            const isAllOrdered = ordered > 0 && ordered >= done
                            const isRestSmall = 0 < done - ordered && done - ordered < 4
                            const color = isAllOrdered
                                ? `error`
                                : isRestSmall
                                ? 'primary'
                                : 'secondary'

                            return (
                                <Grid item key={dish._id}>
                                    <BadgeInfo
                                        count={done - ordered}
                                        isAllOrdered={isAllOrdered}
                                        borderColor={titleObj.color}
                                        badgeColor={color}
                                    >
                                        <Card
                                            onClick={handleAdd(dish)}
                                            square
                                            variant='outlined'
                                            classes={{ root: classes.Card }}
                                            className={color}
                                        >
                                            <CardActionArea>
                                                <CardContent classes={{ root: classes.Content }}>
                                                    <Typography variant='h6'>
                                                        {dish.cardTitle}
                                                    </Typography>
                                                    <Typography
                                                        variant='caption'
                                                        classes={{ root: classes.TypoDone }}
                                                    >
                                                        Выпуск: {done > 0 ? `${done}` : '-'}
                                                    </Typography>
                                                    <Typography variant='caption'>
                                                        {outlook > 0 ? ` (${outlook})` : ''}
                                                    </Typography>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </BadgeInfo>
                                </Grid>
                            )
                        })}
                    </Grid>
                </Paper>
            )
    )
}

export default MenuPanelItem
