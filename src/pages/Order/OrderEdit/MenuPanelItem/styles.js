import { makeStyles } from '@material-ui/core/styles'
import { red, teal, deepOrange, brown, amber, cyan, blueGrey } from '@material-ui/core/colors'
import { green, yellow } from '@material-ui/core/colors'

export default makeStyles((theme) => ({
    Paper: {
        padding: theme.spacing(1),
        '&.red': {
            backgroundColor: red[100],
        },
        '&.teal': {
            backgroundColor: teal[100],
        },
        '&.deepOrange': {
            backgroundColor: deepOrange[100],
        },
        '&.brown': {
            backgroundColor: brown[100],
        },
        '&.amber': {
            backgroundColor: amber[100],
        },
        '&.cyan': {
            backgroundColor: cyan[100],
        },
        '&.blueGrey': {
            backgroundColor: blueGrey[100],
        },
    },
    Card: {
        '&.primary': {
            backgroundColor: yellow[50],
            border: `1px solid ${yellow[200]}`,
        },
        '&.secondary': {
            backgroundColor: green[50],
            border: `1px solid ${green[200]}`,
        },
        '&.error': {
            backgroundColor: red[50],
            border: `1px solid ${red[200]}`,
        },
    },
    Content: {
        padding: theme.spacing(1, 2, 0.5),
    },
    TypoDone: {
        color: theme.palette.grey[700],
        fontWeight: theme.typography.fontWeightMedium,
    },
}))
