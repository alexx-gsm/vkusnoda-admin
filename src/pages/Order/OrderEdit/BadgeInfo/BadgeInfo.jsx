import React from 'react'
// @material-ui
import Badge from '@material-ui/core/Badge'
// styles
import useStyles from './styles'

function BadgeInfo({ count, isAllOrdered, borderColor, badgeColor, children }) {
    const classes = useStyles()

    return (
        <Badge
            showZero={isAllOrdered}
            badgeContent={count}
            color={badgeColor}
            classes={{ root: classes.Root, badge: classes.Badge }}
            className={borderColor}
        >
            {children}
        </Badge>
    )
}

export default BadgeInfo
