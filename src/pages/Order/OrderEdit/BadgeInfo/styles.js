import { makeStyles } from '@material-ui/core/styles'
import { red, teal, deepOrange, brown, amber, cyan, blueGrey } from '@material-ui/core/colors'

export default makeStyles((theme) => ({
    Root: {
        '& $Badge': {
            height: theme.spacing(4),
            minWidth: theme.spacing(4),
            right: theme.spacing(1),
            top: theme.spacing(1),
            padding: theme.spacing(0, 0.5),
            border: `2px solid`,
            borderRadius: theme.spacing(4),
        },
        '&.red $Badge': { borderColor: red[100] },
        '&.teal $Badge': { borderColor: teal[100] },
        '&.deepOrange $Badge': { borderColor: deepOrange[100] },
        '&.brown $Badge': { borderColor: brown[100] },
        '&.amber $Badge': { borderColor: amber[100] },
        '&.cyan $Badge': { borderColor: cyan[100] },
        '&.blueGrey $Badge': { borderColor: blueGrey[100] },
    },
    Badge: {},
}))
