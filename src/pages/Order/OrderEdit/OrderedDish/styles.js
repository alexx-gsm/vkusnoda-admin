import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    PaperRow: {
        margin: theme.spacing(0.5),
        padding: theme.spacing(1),
        backgroundColor: theme.palette.grey[200],
    },
    Garnish: {
        color: theme.palette.grey[900],
        fontWeight: theme.typography.fontWeightLight,
        fontSize: '1rem',
        borderTop: `1px dotted ${theme.palette.grey[200]}`,
        backgroundColor: theme.palette.grey[200],
        padding: theme.spacing(0.5),
        textTransform: 'lowercase',
    },
    TypoAmount: {
        padding: theme.spacing(0, 1),
        minWidth: theme.spacing(8),
    },
    PaperDish: {
        padding: theme.spacing(0.5),
    },
}))
