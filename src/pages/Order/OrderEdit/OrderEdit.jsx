import React, { Fragment, useState, useEffect, useContext } from 'react'
import { Redirect, useParams, Link } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
import format from 'date-fns/format'
// context
import { AppContext } from '../../../contexts/app'
// hook
import useSocket from '../../../hooks/useSocket'
// @material-ui
import { Paper, Box, Container, Grid } from '@material-ui/core'
import { TextField, Typography, Button } from '@material-ui/core'
import { FormControl, FormHelperText } from '@material-ui/core'
import { Select, MenuItem, InputLabel } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
// components
import DateSelect from '../../../components/DateSelect'
import OrderedDishes from './OrderedDishes'
// styles
import useStyles from './styles'

import initialItem from './initialItem'

const baseUrl = '/orders'

function OrderEdit() {
    const classes = useStyles()
    const [{ socket }] = useSocket()

    /** form data */
    const [data, setData] = useState(initialItem)

    /** fetch data from context */
    const [{ orders, clients, couriers }] = useContext(AppContext)

    /** fetch item by '_id' */
    const { _id } = useParams()
    useEffect(() => {
        if (!_id || isEmpty(orders)) {
            return
        }

        const item = orders.find((c) => c._id === _id)
        setData({
            ...item,
            date: new Date(item.date),
        })
    }, [_id, orders, setData])

    /** populate autocomplete's options */
    const [_clients, setClients] = useState({})
    useEffect(() => {
        if (isEmpty(clients)) {
            return
        }
        setClients({
            options: clients.filter((c) => !c.isGroup),
            getOptionLabel: (option) => option.name,
            renderOption: (option) => (
                <Grid container direction='column'>
                    <Typography variant='h6' noWrap>
                        {option.name}
                    </Typography>
                    <Grid container alignItems='baseline' spacing={1}>
                        <Grid item>
                            <Typography variant='subtitle1'>{option.organization}</Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant='caption'>{`(tel: ${option.phones.join(
                                ', '
                            )})`}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            ),
        })
    }, [clients, setClients])

    /** get autocomplete option */
    const getClientOption = (id) => {
        const item = _clients.options.find((item) => item._id === id)
        return item ? item : null
    }

    /** form handlers */
    const handleInput = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        })
    }

    const handleAutocomplete = (e, item) => {
        if (item) {
            setData({
                ...data,
                client: item._id,
                address: item.address,
                organization: item.organization,
                phones: item.phones,
                courier: item.courier,
                payment: item.payment,
                discount: item.discount,
            })
        } else if (item === null) {
            setData({
                ...data,
                client: null,
                address: '',
                organization: '',
                phones: '',
                courier: '',
                payment: '',
                discount: '',
            })
        }
    }

    const handleDateChange = (date) => {
        setData({
            ...data,
            date,
            dishes: [],
        })
    }

    const handleOrderedDishes = (dishes) => {
        setData({
            ...data,
            dishes,
        })
    }

    const getClientNumber = (id) => {
        const client = clients.find((c) => c._id === id)
        return `${format(new Date(data.date), 'yy')}-${client.number}-`
    }

    /** save/update order */
    const [isSaved, setIsSaved] = useState(false)
    const [errors, setErrors] = useState({})

    /** submit handler */
    const handleSubmit = () => {
        socket.emit(
            'save',
            {
                model: 'Order',
                eventName: 'orders',
                data: { ...data, status: 'Новый' },
                options: !data._id
                    ? {
                          hasNumber: true,
                          filter: { client: data.client },
                          preNumber: getClientNumber(data.client),
                      }
                    : {},
            },
            (res) => (res.errors ? setErrors(res.errors) : setIsSaved(true))
        )
    }
    const handleDraftSubmit = () => {
        socket.emit('save', { model: 'Order', eventName: 'orders', data }, (res) =>
            res.errors ? setErrors(res.errors) : setIsSaved(true)
        )
    }

    if (isSaved) {
        return <Redirect to={baseUrl} />
    }

    return (
        <Container maxWidth={false} className={classes.Container}>
            <Paper variant='outlined'>
                <Box className={classes.WrapperHeader}>
                    <Grid
                        container
                        spacing={2}
                        wrap='nowrap'
                        justify='space-between'
                        alignItems='flex-end'
                    >
                        {/* CLIENT */}
                        <Grid item xs={8} sm={6}>
                            {!isEmpty(_clients) && (
                                <Autocomplete
                                    {..._clients}
                                    id='clients'
                                    autoComplete
                                    autoHighlight
                                    value={getClientOption(data.client)}
                                    onChange={handleAutocomplete}
                                    renderInput={(params) => {
                                        return (
                                            <TextField
                                                {...params}
                                                label='Клиент'
                                                required
                                                margin='dense'
                                                fullWidth
                                                classes={{ root: classes.TypographyMain }}
                                                error={Boolean(errors.client)}
                                                helperText={errors.client}
                                            />
                                        )
                                    }}
                                />
                            )}
                        </Grid>
                        {/* CLIENT INFO */}
                        <Grid item xs>
                            <Fragment>
                                <Typography
                                    variant='subtitle1'
                                    classes={{ root: classes.TypographySecondary }}
                                >
                                    <em>орг:</em>
                                    {data.organization}
                                </Typography>
                                <Typography
                                    variant='subtitle1'
                                    classes={{ root: classes.TypographySecondary }}
                                >
                                    <em>тел:</em>
                                    {data.phones ? data.phones.join(', ') : ''}
                                </Typography>
                            </Fragment>
                        </Grid>
                        {/* DATE */}
                        <Grid item xs={2} sm={3}>
                            <DateSelect date={data.date} handleDateChange={handleDateChange} />
                        </Grid>
                    </Grid>
                    {/* ADDRESS */}
                    <Grid container spacing={2} wrap='nowrap' alignItems='center'>
                        <Grid item xs={6} sm={4}>
                            <TextField
                                value={data.address}
                                onChange={handleInput}
                                name='address'
                                margin='dense'
                                id='address'
                                label='Адрес'
                                fullWidth
                                required
                                error={Boolean(errors.address)}
                                helperText={errors.address}
                            />
                        </Grid>
                        {/* COURIER */}
                        <Grid item xs={3} sm={2}>
                            {!isEmpty(couriers) && (
                                <FormControl
                                    margin='dense'
                                    fullWidth
                                    error={!isEmpty(errors.courier)}
                                >
                                    <InputLabel id='couriers' required>
                                        Курьер
                                    </InputLabel>
                                    <Select
                                        labelId='couriers'
                                        id='couriers-select'
                                        name='courier'
                                        value={data.courier}
                                        onChange={handleInput}
                                    >
                                        {couriers.map(({ _id, title }) => (
                                            <MenuItem key={_id} value={_id}>
                                                {title}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                    {errors.courier && <FormHelperText>Error</FormHelperText>}
                                </FormControl>
                            )}
                        </Grid>
                        {/* PAYMENTS */}
                        <Grid item xs={3} sm={2}>
                            <FormControl margin='dense' fullWidth>
                                <InputLabel id='payment' required>
                                    Тип оплаты
                                </InputLabel>
                                <Select
                                    labelId='payment'
                                    id='payment-select'
                                    name='payment'
                                    value={data.payment}
                                    onChange={handleInput}
                                >
                                    <MenuItem value='cash'>Наличные</MenuItem>
                                    <MenuItem value='card'>Карта</MenuItem>
                                    <MenuItem value='account'>Безнал</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        {/* DISCOUNT */}
                        <Grid item xs={2} sm={1}>
                            <TextField
                                value={data.discount}
                                onChange={handleInput}
                                type='number'
                                name='discount'
                                margin='dense'
                                id='discount'
                                label='Скидка'
                                fullWidth
                                required
                                InputProps={{
                                    endAdornment: '%',
                                }}
                                error={Boolean(errors.discount)}
                                helperText={errors.discount}
                            />
                        </Grid>
                        {/* STATUS */}
                        <Grid item xs>
                            <Typography
                                variant='h6'
                                align='center'
                                classes={{ root: classes.Status }}
                            >
                                {data.status}
                            </Typography>
                        </Grid>
                    </Grid>
                </Box>
                {/* ORDERED_DISHES */}
                <OrderedDishes
                    date={data.date}
                    setData={setData}
                    orderedDishes={data.dishes}
                    setOrderedDishes={handleOrderedDishes}
                />
                {/* ACTIONS */}
                {/* <FormFooter {...{ baseUrl, handleSubmit }} /> */}
                <Box className={classes.Footer}>
                    <Button to={baseUrl} color='default' component={Link}>
                        Отмена
                    </Button>
                    {data.status === 'Черновик' && (
                        <Button
                            onClick={handleDraftSubmit}
                            variant='outlined'
                            color='secondary'
                            className={classes.Button}
                        >
                            Черновик
                        </Button>
                    )}
                    <Button onClick={handleSubmit} variant='contained' color='primary'>
                        Сохранить
                    </Button>
                </Box>
            </Paper>
        </Container>
    )
}

export default OrderEdit
