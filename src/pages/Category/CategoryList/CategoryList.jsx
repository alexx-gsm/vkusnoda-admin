import React, { useState, useEffect, useContext } from 'react'
import { useHistory } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
// context
import { AppContext } from '../../../contexts/app'
// @material-ui
import { Container, Paper } from '@material-ui/core'
// components
import Header from '../../../components/common/Header'
import StyledTree from '../../../components/common/StyledTree/StyledTree'
// styles
import useStyles from './styles'

const baseUrl = '/categories'

function CategoryList() {
    const classes = useStyles()
    const history = useHistory()

    const [filter, setFilter] = useState('')
    const [filteredItems, setFilteredItems] = useState([])

    const [{ categories }] = useContext(AppContext)

    useEffect(() => {
        if (isEmpty(filter) || isEmpty(categories)) {
            return setFilteredItems([])
        }

        const filteredItems = categories.filter(
            (category) => category.title.toLowerCase().search(filter.toLowerCase()) !== -1
        )

        setFilteredItems(filteredItems)
    }, [filter, categories, setFilteredItems])

    return (
        <Container maxWidth='sm' classes={{ root: classes.Container }}>
            <Paper classes={{ root: classes.Paper }}>
                <Header
                    title='Категории'
                    filter={filter}
                    setFilter={setFilter}
                    disabled={isEmpty(categories)}
                    handleClick={() => history.push(`${baseUrl}/edit/`)}
                />
                <StyledTree
                    items={categories}
                    isFilter={!isEmpty(filter)}
                    filteredItems={filteredItems}
                    getTitle={(category) => category.title}
                    handleClick={(_id) => () => history.push(`${baseUrl}/edit/${_id}`)}
                />
            </Paper>
        </Container>
    )
}

export default CategoryList
