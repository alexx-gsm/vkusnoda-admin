import React, { useState, useEffect, useContext } from 'react'
import { Redirect, useParams } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
// context
import { AppContext } from '../../../contexts/app'
// hook
import useSocket from '../../../hooks/useSocket'
// @material-ui
import { Paper, Box, Container, Grid } from '@material-ui/core'
import { TextField } from '@material-ui/core'
import { FormControlLabel } from '@material-ui/core'
import { Switch } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
// components
import FormHeader from '../../../components/FormHeader'
import FormFooter from '../../../components/FormFooter'
// styles
import useStyles from './styles'

import initialItem from './initialItem'

const baseUrl = '/categories'

function CategoryEdit() {
    const classes = useStyles()
    const [{ socket }] = useSocket()

    /** form data */
    const [data, setData] = useState(initialItem)

    /** fetch data from context */
    const [{ categories }] = useContext(AppContext)

    /** fetch item by '_id' */
    const { _id } = useParams()
    useEffect(() => {
        if (!_id || isEmpty(categories)) {
            return
        }

        const item = categories.find((c) => c._id === _id)
        setData(item)
    }, [_id, categories])

    /** populate autocomplete's options */
    const [_categories, setCategories] = useState({})
    useEffect(() => {
        setCategories({
            options: categories.filter((c) => c.isGroup),
            getOptionLabel: (option) => option.title,
        })
    }, [categories, setCategories])

    /** form handlers */
    const handleInput = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        })
    }

    const handleChange = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.checked,
        })
    }

    const handleAutocomplete = (e, val) => {
        setData({
            ...data,
            parent: val ? val._id : null,
        })
    }

    /** save/update category */
    const [isSaved, setIsSaved] = useState(false)
    const [errors, setErrors] = useState({})

    /** submit handler */
    const handleSubmit = () => {
        socket.emit('save', { model: 'Category', eventName: 'categories', data }, (res) =>
            res.errors ? setErrors(res.errors) : setIsSaved(true)
        )
    }

    if (isSaved) {
        return <Redirect to={baseUrl} />
    }

    const getCategory = (id) => {
        const item = _categories.options.find((item) => item._id === id)
        return item ? item : null
    }

    return (
        <Container maxWidth='sm' classes={{ root: classes.Container }}>
            <Paper variant='outlined'>
                <FormHeader title={data._id ? 'Категория' : 'Новая категория'} />
                <Box className={classes.Wrapper}>
                    <TextField
                        autoFocus
                        value={data.title}
                        onChange={handleInput}
                        name='title'
                        margin='normal'
                        id='title'
                        label='Название'
                        type='text'
                        fullWidth
                        error={Boolean(errors.title)}
                        helperText={errors.title}
                        InputProps={{
                            classes: { input: classes.Title },
                        }}
                    />
                    <Grid container alignItems='flex-end' spacing={2}>
                        <Grid item>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={data.isGroup}
                                        onChange={handleChange}
                                        name='isGroup'
                                        color='primary'
                                    />
                                }
                                label='Группа'
                            />
                        </Grid>
                        <Grid item xs>
                            {!isEmpty(_categories) && (
                                <Autocomplete
                                    {..._categories}
                                    id='categories'
                                    autoComplete
                                    disabled={isEmpty(categories)}
                                    value={getCategory(data.parent)}
                                    onChange={handleAutocomplete}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            fullWidth
                                            label='Родитель'
                                            margin='normal'
                                            error={Boolean(errors.parent)}
                                            helperText={errors.parent}
                                        />
                                    )}
                                />
                            )}
                        </Grid>
                        <Grid item>
                            <TextField
                                value={data.sorting}
                                onChange={handleInput}
                                name='sorting'
                                id='sorting'
                                label='Sort'
                                type='number'
                                margin='normal'
                                required
                                fullWidth
                                error={Boolean(errors.sorting)}
                                helperText={errors.sorting}
                            />
                        </Grid>
                    </Grid>
                </Box>
                <FormFooter {...{ baseUrl, handleSubmit }} />
            </Paper>
        </Container>
    )
}

export default CategoryEdit
