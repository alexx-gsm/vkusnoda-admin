import React, { useState, useEffect, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import jwt_decode from 'jwt-decode'
// @material-ui
import { Dialog, DialogContent, DialogActions } from '@material-ui/core'
import { Container, TextField, Button } from '@material-ui/core'
// context
import { AuthContext, SET_AUTHORIZED } from '../../contexts/auth'
// hooks
import useFetch from '../../hooks/useFetch'
import useLocalStorage from '../../hooks/useLocalStorage'
// styles
import useStyles from './styles'

function Login() {
    const classes = useStyles()
    const [open, setOpen] = useState(false)
    const [data, setData] = useState({})
    const { email = '', password = '' } = data
    const [isSubmitted, setIsSubmitted] = useState(false)
    const [{ isLoading, response }, doFetch] = useFetch('/users/login')
    const [token, setToken] = useLocalStorage('token')

    const [, dispatch] = useContext(AuthContext)

    useEffect(() => {
        const timer = setTimeout(() => setOpen(true), 500)

        return () => clearTimeout(timer)
    }, [open])

    useEffect(() => {
        if (!response) {
            return
        }

        setToken(response.token)
    }, [response, setToken])

    useEffect(() => {
        if (!token || !response) {
            return
        }

        dispatch({ type: SET_AUTHORIZED, payload: jwt_decode(response.token) })
        setIsSubmitted(true)
    }, [token, response, dispatch])

    const handleInput = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        })
    }

    const handleSubmit = () => {
        doFetch({
            method: 'POST',
            data: { email, password },
        })
    }

    if (isSubmitted) {
        return <Redirect to='/' />
    }

    return (
        <Container maxWidth={false} classes={{ root: classes.Container }}>
            <Dialog
                open={open}
                disableBackdropClick
                aria-labelledby='title'
                fullWidth
                maxWidth='xs'
            >
                <DialogContent>
                    <TextField
                        value={email}
                        onChange={handleInput}
                        name='email'
                        autoFocus
                        margin='normal'
                        id='email'
                        label='Email'
                        type='email'
                        fullWidth
                    />
                    <TextField
                        value={password}
                        onChange={handleInput}
                        name='password'
                        margin='normal'
                        id='password'
                        label='Password'
                        type='password'
                        fullWidth
                    />
                </DialogContent>
                <DialogActions classes={{ root: classes.Actions }}>
                    <Button
                        onClick={handleSubmit}
                        disabled={isLoading}
                        variant='contained'
                        color='primary'
                        fullWidth
                        classes={{ root: classes.Subbmit }}
                    >
                        Login
                    </Button>
                </DialogActions>
            </Dialog>
        </Container>
    )
}

export default Login
