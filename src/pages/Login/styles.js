import { makeStyles } from '@material-ui/core/styles'
import bgImage from './bg.jpg'

export default makeStyles((theme) => ({
    Container: {
        backgroundImage: `url(${bgImage})`,
        backgroundSize: 'cover',
        width: '100vw',
        height: '100vh',
    },
    Actions: {
        padding: theme.spacing(2, 3),
    },
}))
