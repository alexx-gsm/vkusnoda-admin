import React, { useState, useEffect, useContext } from 'react'
import { Redirect, useParams } from 'react-router-dom'
import isEmpty from '../../../helpers/is-empty'
// context
import { AppContext } from '../../../contexts/app'
// hook
import useSocket from '../../../hooks/useSocket'
// @material-ui
import { Container, Paper, Box } from '@material-ui/core'
import { TextField } from '@material-ui/core'
// components
import FormHeader from '../../../components/FormHeader'
import FormFooter from '../../../components/FormFooter'
// styles
import useStyles from './styles'

import initialItem from './initialItem'

const baseUrl = '/couriers'

function CourierEdit() {
    const classes = useStyles()
    const [{ socket }] = useSocket()

    /** form data */
    const [data, setData] = useState(initialItem)

    /** fetch data from context */
    const [{ couriers }] = useContext(AppContext)

    /** fetch item by '_id' */
    const { _id } = useParams()
    useEffect(() => {
        if (!_id || isEmpty(couriers)) {
            return
        }

        const item = couriers.find((i) => i._id === _id)
        setData(item)
    }, [_id, couriers])

    /** form handlers */
    const handleInput = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        })
    }

    /** save/update item */
    const [isSaved, setIsSaved] = useState(false)
    const [errors, setErrors] = useState({})

    /** submit handler */
    const handleSubmit = () => {
        socket.emit('save', { model: 'Courier', eventName: 'couriers', data }, (res) =>
            res.errors ? setErrors(res.errors) : setIsSaved(true)
        )
    }

    if (isSaved) {
        return <Redirect to={baseUrl} />
    }

    return (
        <Container maxWidth='sm' classes={{ root: classes.Container }}>
            <Paper variant='outlined'>
                <FormHeader title={data._id ? 'Курьер' : 'Новый курьер'} />

                <Box className={classes.Wrapper}>
                    <TextField
                        value={data.title}
                        onChange={handleInput}
                        name='title'
                        autoFocus
                        margin='normal'
                        id='title'
                        label='Название'
                        type='text'
                        fullWidth
                        InputProps={{
                            classes: { input: classes.Title },
                        }}
                        error={Boolean(errors.title)}
                        helperText={errors.title}
                    />
                </Box>
                <FormFooter {...{ baseUrl, handleSubmit }} />
            </Paper>
        </Container>
    )
}

export default CourierEdit
