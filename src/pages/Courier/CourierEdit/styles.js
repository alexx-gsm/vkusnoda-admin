import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Container: {
        padding: 0,
        marginLeft: 0,
    },
    Wrapper: {
        padding: theme.spacing(1, 2),
    },
    Title: {
        ...theme.typography.title,
    },
}))
