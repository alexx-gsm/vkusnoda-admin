import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom'
// context
import { AppContext } from '../../../contexts/app'
// material-table
import MaterialTable, { MTableToolbar } from 'material-table'
// material-ui
import { Container } from '@material-ui/core'
// components
import TableTitle from '../../../components/TableTitle'
// styles
import useStyles from './styles'

const baseUrl = '/couriers'

function CourierList() {
    const classes = useStyles()
    const history = useHistory()
    const [{ couriers }] = useContext(AppContext)

    const handleRowClick = (e, rowData) => history.push(`${baseUrl}/edit/${rowData._id}`)

    return (
        <Container maxWidth='sm' classes={{ root: classes.Container }}>
            <MaterialTable
                title={TableTitle('Курьеры')}
                columns={[{ title: 'Название', field: 'title' }]}
                data={couriers}
                onRowClick={handleRowClick}
                options={{
                    sorting: true,
                    pageSize: 10,
                    pageSizeOptions: [5, 10, 50],
                    headerStyle: {
                        background: 'whitesmoke',
                    },
                }}
                actions={[
                    {
                        icon: 'add_circle',
                        tooltip: 'Add new Courier',
                        isFreeAction: true,
                        iconProps: {
                            classes: { root: classes.AddButton },
                            fontSize: 'large',
                        },
                        onClick: () => history.push(`${baseUrl}/edit/`),
                    },
                ]}
                components={{
                    Toolbar: (props) => (
                        <MTableToolbar {...props} classes={{ root: classes.Toolbar }} />
                    ),
                }}
            />
        </Container>
    )
}

export default CourierList
