import { startOfDay, endOfDay } from 'date-fns'

export const getDayInterval = (date) => ({
    $gte: startOfDay(date),
    $lt: endOfDay(date),
})
