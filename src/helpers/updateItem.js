const updateItem = (item, items) => {
    const index = items.findIndex((i) => i._id === item._id)
    return index === -1
        ? [...items, item]
        : [...items.slice(0, index), item, ...items.slice(index + 1)]
}

export default updateItem
