const formatter = new Intl.NumberFormat('ru-ru', {
    style: 'currency',
    currency: 'RUB',
    minimumFractionDigits: 0,
})

export default formatter
