import { DISH_CATEGORY_TITLE } from '../config/constants'

const sortDishCategories = (categories) => {
    const dishCategory = categories.find((category) => category.title === DISH_CATEGORY_TITLE)

    return categories
        .filter((category) => category.parent === dishCategory._id)
        .sort((a, b) => a.sorting - b.sorting)
}

export default sortDishCategories
