/**
 * Save item
 * @param {object} options
 * @param {string} options.model - MongoDB's Model name to save item
 * @param {string} options.eventName - socket event name to fire event after successful saving
 * @param {object} options.data - item to save
 * @param {function} cb - callback function to return result of saving
 */
export const saveItem = (socket, options, cb) => {
    socket.emit('save', options, cb)
}
