import models from '../models'
import { UPDATE_ITEM, SET_ITEMS } from '../contexts/app'

export default (socket, dispatch) => {
    models.map(({ model, eventName, autoload = true, options = {} }) => {
        const setItems = (items) => {
            dispatch({
                type: SET_ITEMS,
                payload: { [eventName]: items },
            })
        }

        const updateItem = (item) => {
            dispatch({
                type: UPDATE_ITEM,
                payload: { eventName, item },
            })
        }

        socket.on(eventName, setItems)
        socket.on(`updated_${eventName}`, updateItem)
        socket.emit(`getItemsOf${model}`, options, setItems)
        return undefined
    })
}
