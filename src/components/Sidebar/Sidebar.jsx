import React from 'react'
import { NavLink, Link } from 'react-router-dom'
import clsx from 'clsx'
import isEmpty from '../../helpers/is-empty'
// @material-ui
import { Drawer, Divider } from '@material-ui/core'
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { Typography, IconButton } from '@material-ui/core'
// @material-icon
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
// links
import links from './links'
// styles
import useStyles from './styles'

function Sidebar({ open, toggleOpen }) {
    const classes = useStyles()

    return (
        <Drawer
            variant='permanent'
            className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
            })}
            classes={{
                paper: clsx({
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                }),
            }}
        >
            <div className={classes.toolbar}>
                <Typography variant='h6' className={classes.Title}>
                    <Link to='/' color='primary' className={classes.TitleLink}>
                        ВкусноДа!
                    </Link>
                </Typography>
                <IconButton onClick={toggleOpen} classes={{ root: classes.TitleIcon }}>
                    <ChevronLeftIcon />
                </IconButton>
            </div>
            <Divider />
            <List component='nav' aria-label='sidebar' disablePadding>
                {!isEmpty(links) &&
                    links.map(({ title, url, icon }, index) =>
                        title === 'divider' ? (
                            <Divider key={index} />
                        ) : (
                            <ListItem button key={url} component={NavLink} to={url}>
                                <ListItemIcon>{icon}</ListItemIcon>
                                <ListItemText primary={title} />
                            </ListItem>
                        )
                    )}
            </List>
        </Drawer>
    )
}

export default Sidebar
