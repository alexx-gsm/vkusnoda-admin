import { makeStyles } from '@material-ui/core/styles'
import { DRAWERWIDTH } from '../../config/constants'

export default makeStyles((theme) => ({
    drawer: {
        width: DRAWERWIDTH,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: DRAWERWIDTH,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        backgroundColor: theme.palette.primary.main,
    },
    Title: {},
    TitleLink: {
        textDecoration: 'none',
        color: theme.palette.common.white,
    },
    TitleIcon: {
        color: theme.palette.common.white,
    },
}))
