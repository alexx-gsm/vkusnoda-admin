import React from 'react'
// @material-icons
import DashboardIcon from '@material-ui/icons/Dashboard'
import DirectionsBikeIcon from '@material-ui/icons/DirectionsBike'
import PeopleAltIcon from '@material-ui/icons/PeopleAlt'
import LocalOfferIcon from '@material-ui/icons/LocalOffer'
import CategoryIcon from '@material-ui/icons/Category'
import LocalCafeIcon from '@material-ui/icons/LocalCafe'
import ReceiptIcon from '@material-ui/icons/Receipt'
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow'
import StorageIcon from '@material-ui/icons/Storage'
import RoomServiceIcon from '@material-ui/icons/RoomService'

export default [
    {
        title: 'Dashboard',
        url: '/',
        icon: <DashboardIcon />,
    },
    {
        title: 'divider',
    },
    {
        title: 'Заказы',
        url: '/orders',
        icon: <RoomServiceIcon />,
    },
    {
        title: 'Меню',
        url: '/menus',
        icon: <ReceiptIcon />,
    },
    {
        title: 'Выпуски',
        url: '/outputs',
        icon: <DoubleArrowIcon />,
    },
    {
        title: 'divider',
    },
    {
        title: 'Склады',
        url: '/warehouses',
        icon: <StorageIcon />,
    },
    {
        title: 'divider',
    },
    {
        title: 'Клиенты',
        url: '/clients',
        icon: <PeopleAltIcon />,
    },
    {
        title: 'divider',
    },
    {
        title: 'Блюда',
        url: '/dishes',
        icon: <LocalCafeIcon />,
    },
    {
        title: 'Категории',
        url: '/categories',
        icon: <LocalOfferIcon />,
    },
    {
        title: 'UOMs',
        url: '/uoms',
        icon: <CategoryIcon />,
    },
    {
        title: 'divider',
    },
    {
        title: 'Курьеры',
        url: '/couriers',
        icon: <DirectionsBikeIcon />,
    },
]
