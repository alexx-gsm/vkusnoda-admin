import React from 'react'
// @material-lab
import { TreeView } from '@material-ui/lab'
// @material-icon
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import ArrowRightIcon from '@material-ui/icons/ArrowRight'
import FolderOpenIcon from '@material-ui/icons/FolderOpen'
import FolderIcon from '@material-ui/icons/Folder'
// components
import StyledTreeList from './StyledTreeList'
import StyledTreeItem from './StyledTreeItem'
// styles
import useStyles from './styles'

function StyledTree({
    items,
    isFilter,
    filteredItems,
    FilteredItemIcon = PlayArrowIcon,
    ItemIcon = PlayArrowIcon,
    iconEmptyGroup = FolderOpenIcon,
    iconNotEmptyGroup = FolderIcon,
    getTitle,
    handleClick,
}) {
    const classes = useStyles()

    return (
        <TreeView
            className={classes.root}
            defaultCollapseIcon={<ArrowDropDownIcon />}
            defaultExpandIcon={<ArrowRightIcon />}
            defaultEndIcon={<div style={{ width: 24 }} />}
            defaultExpanded={['0']}
        >
            {isFilter ? (
                filteredItems.map((item) => (
                    <StyledTreeItem
                        key={item._id}
                        nodeId={item._id}
                        labelText={getTitle(item)}
                        labelIcon={FilteredItemIcon}
                        bgColor={'whitesmoke'}
                        handleClick={handleClick(item._id)}
                        isGroup={false}
                    />
                ))
            ) : (
                <StyledTreeList
                    items={items}
                    handleClick={handleClick}
                    ItemIcon={ItemIcon}
                    iconEmptyGroup={iconEmptyGroup}
                    iconNotEmptyGroup={iconNotEmptyGroup}
                />
            )}
        </TreeView>
    )
}

export default StyledTree
