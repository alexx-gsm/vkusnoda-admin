import React from 'react'
// @material-lab
import { TreeItem } from '@material-ui/lab'
// @material-ui
import { Typography, IconButton } from '@material-ui/core'
// @material-icon
import EditIcon from '@material-ui/icons/Edit'
// styles
import useStyles from './styles'

function StyledTreeItem(props) {
    const classes = useStyles()
    const {
        labelText,
        labelIcon: LabelIcon,
        color,
        bgColor,
        isGroup = false,
        handleClick,
        ...other
    } = props

    return (
        <TreeItem
            onClick={!isGroup ? handleClick : null}
            label={
                <div className={classes.labelRoot}>
                    <LabelIcon color='inherit' className={classes.labelIcon} />
                    <Typography variant='body2' className={classes.labelText}>
                        {labelText}
                    </Typography>
                    {isGroup && (
                        <IconButton
                            onClick={handleClick}
                            aria-label='edit'
                            className={classes.margin}
                            size='small'
                        >
                            <EditIcon fontSize='inherit' />
                        </IconButton>
                    )}
                </div>
            }
            style={{
                '--tree-view-color': color,
                '--tree-view-bg-color': bgColor,
            }}
            classes={{
                root: classes.root,
                content: classes.content,
                expanded: classes.expanded,
                group: classes.group,
                label: classes.label,
            }}
            {...other}
        />
    )
}

export default StyledTreeItem
