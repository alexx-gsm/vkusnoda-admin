import React from 'react'
import isEmpty from '../../../../helpers/is-empty'
// components
import StyledTreeItem from '../StyledTreeItem'
// @material-icon
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import FolderOpenIcon from '@material-ui/icons/FolderOpen'
import FolderIcon from '@material-ui/icons/Folder'

function StyledTreeList({
    items,
    handleClick,
    ItemIcon = PlayArrowIcon,
    iconEmptyGroup = FolderOpenIcon,
    iconNotEmptyGroup = FolderIcon,
}) {
    const renderTree = (groupId = '') => {
        return [
            ...items
                .filter((item) => item.isGroup && item.parent === groupId)
                .map((group) => {
                    const children = items.filter((item) => item.parent === group._id)
                    return (
                        <StyledTreeItem
                            key={group._id}
                            nodeId={group._id}
                            labelText={group.title || group.organization}
                            labelIcon={children.length === 0 ? iconEmptyGroup : iconNotEmptyGroup}
                            bgColor={!isEmpty(children) ? 'whitesmoke' : 'inherit'}
                            handleClick={handleClick(group._id)}
                            isGroup={true}
                        >
                            {!isEmpty(children) ? renderTree(group._id) : null}
                        </StyledTreeItem>
                    )
                }),
            ...items
                .filter((item) => !item.isGroup && item.parent === groupId)
                .sort((a, b) => a.sorting - b.sorting)
                .map((item) => {
                    return (
                        <StyledTreeItem
                            key={item._id}
                            nodeId={item._id}
                            labelText={item.title || item.name}
                            labelIcon={ItemIcon}
                            bgColor={'whitesmoke'}
                            handleClick={handleClick(item._id)}
                            isGroup={false}
                        />
                    )
                }),
        ]
    }

    return renderTree()
}

export default StyledTreeList
