import React from 'react'
import { Typography } from '@material-ui/core'
import useStyles from './styles'

function HeaderTitle({ title }) {
    const classes = useStyles()

    return (
        <Typography variant='h4' classes={{ root: classes.Title }}>
            {title}
        </Typography>
    )
}

export default HeaderTitle
