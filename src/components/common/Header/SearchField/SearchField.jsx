import React from 'react'
import isEmpty from '../../../../helpers/is-empty'
// @material-ui
import { TextField } from '@material-ui/core'
import { IconButton } from '@material-ui/core'
import { InputAdornment } from '@material-ui/core'
// @material-icon
import SearchIcon from '@material-ui/icons/Search'
import CloseIcon from '@material-ui/icons/Close'
// styles
import useStyles from './styles'

function SearchField({ filter, disabled, handleChange, handleClear }) {
    const classes = useStyles()

    return (
        <TextField
            id='search'
            autoFocus={!isEmpty(filter)}
            className={classes.SearchField}
            value={filter}
            onChange={handleChange}
            disabled={disabled}
            InputProps={{
                startAdornment: (
                    <InputAdornment position='start'>
                        <SearchIcon color='secondary' size='small' />
                    </InputAdornment>
                ),
                endAdornment: (
                    <InputAdornment position='end'>
                        <IconButton
                            aria-label='icon icon-clear'
                            classes={{ root: classes.IconClear }}
                            color='primary'
                            disabled={isEmpty(filter)}
                            onClick={handleClear}
                        >
                            <CloseIcon size='small' />
                        </IconButton>
                    </InputAdornment>
                ),
            }}
        />
    )
}

export default SearchField
