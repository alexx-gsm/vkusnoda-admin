import React from 'react'
// material-ui
import { Paper, Grid } from '@material-ui/core'
// components
import HeaderTitle from './HeaderTitle'
import SearchField from './SearchField'
import AddButton from './AddButton'
// styles
import useStyles from './styles'

function Header({ title, filter, setFilter, disabled, handleClick }) {
    const classes = useStyles()

    const handleChange = (e) => setFilter(e.target.value)
    const handleClear = () => setFilter('')

    return (
        <Paper square variant='outlined' classes={{ root: classes.Paper }}>
            <Grid container spacing={2}>
                <Grid item xs>
                    <HeaderTitle title={title} />
                </Grid>
                <Grid item>
                    <SearchField
                        filter={filter}
                        disabled={disabled}
                        handleChange={handleChange}
                        handleClear={handleClear}
                    />
                </Grid>
                <Grid item>
                    <AddButton handleAddClick={handleClick} />
                </Grid>
            </Grid>
        </Paper>
    )
}

export default Header
