import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Paper: {
        padding: theme.spacing(2),
        backgroundColor: theme.palette.grey[300],
    },
}))
