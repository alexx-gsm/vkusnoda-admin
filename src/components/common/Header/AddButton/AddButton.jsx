import React from 'react'
// @material-ui
import { Fab } from '@material-ui/core'
// @material-icon
import AddIcon from '@material-ui/icons/Add'

function AddButton({ handleAddClick }) {
    return (
        <Fab onClick={handleAddClick} size='small' color='secondary' aria-label='add'>
            <AddIcon />
        </Fab>
    )
}

export default AddButton
