import React from 'react'
import { Typography } from '@material-ui/core'
// styles
import useStyles from './styles'

function FormHeader({ title, align = 'center' }) {
    const classes = useStyles()
    return (
        <Typography align={align} variant='h5' classes={{ root: classes.Title }}>
            {title}
        </Typography>
    )
}

export default FormHeader
