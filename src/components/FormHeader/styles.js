import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Title: {
        padding: theme.spacing(1, 2),
        textTransform: 'uppercase',
        color: theme.palette.secondary.dark,
        backgroundColor: theme.palette.grey[100],
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
}))
