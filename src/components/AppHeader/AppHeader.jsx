import React from 'react'
import Sidebar from '../Sidebar'
import Header from '../Header'

function AppHeader() {
    const [open, setOpen] = React.useState(false)
    const toggleOpen = React.useCallback(() => setOpen(!open), [open])

    return (
        <React.Fragment>
            <Header {...{ open, toggleOpen }} />
            <Sidebar {...{ open, toggleOpen }} />
        </React.Fragment>
    )
}

export default AppHeader
