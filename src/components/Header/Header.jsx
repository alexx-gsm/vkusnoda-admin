import React from 'react'
import clsx from 'clsx'
// @material-ui
import { AppBar, Toolbar } from '@material-ui/core'
import { Box } from '@material-ui/core'
import { Typography, IconButton } from '@material-ui/core'
// @material-icon
import MenuIcon from '@material-ui/icons/Menu'
// components
import SocketConnection from '../SocketConnection'
// styles
import useStyles from './styles'

function Header({ open, toggleOpen }) {
    const classes = useStyles()

    return (
        <AppBar
            position='fixed'
            color='secondary'
            className={clsx(classes.appBar, {
                [classes.appBarShift]: open,
            })}
        >
            <Toolbar>
                <IconButton
                    color='inherit'
                    aria-label='open drawer'
                    onClick={toggleOpen}
                    edge='start'
                    className={clsx(classes.menuButton, {
                        [classes.hide]: open,
                    })}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant='h6' noWrap>
                    CRM
                </Typography>

                <Box classes={{ root: classes.BoxIndicator }}>
                    <SocketConnection />
                </Box>
            </Toolbar>
        </AppBar>
    )
}

export default Header
