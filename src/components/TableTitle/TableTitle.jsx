import React from 'react'
import { Typography } from '@material-ui/core'
// styles
import useStyles from './styles'

const TableTitle = (text) => {
    const classes = useStyles()
    return (
        <Typography variant='h4' classes={{ root: classes.TableTitle }}>
            {text}
        </Typography>
    )
}

export default TableTitle
