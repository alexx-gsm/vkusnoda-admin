import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    TableTitle: {
        color: theme.palette.secondary.dark,
        textTransform: 'uppercase',
    },
}))
