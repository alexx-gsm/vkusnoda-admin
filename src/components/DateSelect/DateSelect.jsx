import React from 'react'
import format from 'date-fns/format'
import { ru } from 'date-fns/locale'
import DateFnsUtils from '@date-io/date-fns'
// @material-pickers
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
// @material-icons
import TodayIcon from '@material-ui/icons/Today'
// styles
import useStyles from './styles'

class RuLocalizedUtils extends DateFnsUtils {
    getCalendarHeaderText(date) {
        return format(date, 'LLLL', { locale: this.locale })
    }
    getDatePickerHeaderText(date) {
        return format(date, 'd MMMM', { locale: this.locale })
    }
}

function DateSelect({ date, handleDateChange }) {
    const classes = useStyles()

    return (
        <MuiPickersUtilsProvider utils={RuLocalizedUtils} locale={ru}>
            <DatePicker
                value={date}
                onChange={handleDateChange}
                format='d MMMM yyyy'
                // minDate={new Date()}
                cancelLabel='Отмена'
                todayLabel='Сегодня'
                showTodayButton
                classes={{ root: classes.Input }}
                InputProps={{
                    endAdornment: <TodayIcon color='secondary' />,
                }}
            />
        </MuiPickersUtilsProvider>
    )
}

export default DateSelect
