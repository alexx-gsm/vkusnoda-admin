import React, { useContext, Fragment } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
// @material-ui
import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/core/styles'
// theme
import theme from './theme'
// pages
import Login from '../../pages/Login'
import AdminLayout from '../../layouts/Admin'
// context
import { AuthContext } from '../../contexts/auth'

function App() {
    const [{ isLoggedIn }] = useContext(AuthContext)

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <Router>
                <Switch>
                    {isLoggedIn ? (
                        <Route path='/' component={AdminLayout} />
                    ) : (
                        <Fragment>
                            <Route path='/login' component={Login} />
                            <Redirect to='/login' />
                        </Fragment>
                    )}
                </Switch>
            </Router>
        </ThemeProvider>
    )
}

export default App
