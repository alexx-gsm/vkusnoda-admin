import { createMuiTheme } from '@material-ui/core/styles'
import { deepOrange, blueGrey, grey } from '@material-ui/core/colors'

export default createMuiTheme({
    palette: {
        primary: deepOrange,
        secondary: blueGrey,
    },
    typography: {
        title: {
            fontFamily: `"Roboto Condensed", "Helvetica", "Arial", sans-serif`,
            color: grey[700],
            fontSize: '3rem',
            lineHeight: 1.167,
            letterSpacing: '0em',
            padding: 0,
        },
    },
})
