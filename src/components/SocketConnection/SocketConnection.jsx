import React, { useEffect, useContext } from 'react'
// @material icon
import ExploreIcon from '@material-ui/icons/Explore'
import ExploreOffIcon from '@material-ui/icons/ExploreOff'
// context
import { AppContext } from '../../contexts/app'
// hooks
import useLocalStorage from '../../hooks/useLocalStorage'
import useSocket from '../../hooks/useSocket'
// subscriber
import subscriber from '../../api/subscriber'

function SocketConnection() {
    const [, dispatch] = useContext(AppContext)

    const [token] = useLocalStorage('token')

    const _token = token ? token.split(' ', 2)[1] : null

    const [{ socket, isConnected }, doConnect] = useSocket()

    useEffect(() => {
        if (_token === null) {
            return
        }

        doConnect(_token)
    }, [doConnect, _token])

    useEffect(() => {
        if (!isConnected) {
            return
        }

        subscriber(socket, dispatch)
    }, [socket, isConnected, dispatch])

    return isConnected ? <ExploreIcon /> : <ExploreOffIcon color='disabled' />
}

export default SocketConnection
