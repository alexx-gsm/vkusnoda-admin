import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import { Alert, AlertTitle } from '@material-ui/lab'
// styles
// import useStyles from './styles'

const AlertWrap = (props) => {
    return <Alert elevation={6} variant='filled' {...props} />
}

function SnackbarMessage({ title, message, severity, duration }) {
    // const classes = useStyles()
    const [open, setOpen] = React.useState(true)
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return
        }

        setOpen(false)
    }

    return (
        <Snackbar open={open} autoHideDuration={duration} onClose={handleClose}>
            <AlertWrap onClose={handleClose} severity={severity}>
                <AlertTitle>{title}</AlertTitle>
                {message}
            </AlertWrap>
        </Snackbar>
    )
}

export default SnackbarMessage
