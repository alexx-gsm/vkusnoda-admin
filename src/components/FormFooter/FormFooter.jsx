import React from 'react'
import { Link } from 'react-router-dom'
import { Box, Button } from '@material-ui/core'
// styles
import useStyles from './styles'

function FormFooter({ baseUrl, handleSubmit }) {
    const classes = useStyles()
    return (
        <Box className={classes.Footer}>
            <Button to={baseUrl} color='default' component={Link}>
                Отмена
            </Button>
            <Button onClick={handleSubmit} variant='contained' color='primary'>
                Сохранить
            </Button>
        </Box>
    )
}

export default FormFooter
