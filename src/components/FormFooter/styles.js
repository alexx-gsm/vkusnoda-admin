import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    Footer: {
        padding: theme.spacing(2),
        display: 'flex',
        justifyContent: 'space-between',
    },
}))
