import React, { useEffect, useContext } from 'react'
import useFetch from '../../hooks/useFetch'
import { AuthContext, LOADING, SET_AUTHORIZED, SET_UNAUTHORIZED } from '../../contexts/auth'
import useLocalStorage from '../../hooks/useLocalStorage'

function CurrentUserChecker({ children }) {
    const [{ response }, doFetch] = useFetch('/users/current')
    const [, dispatch] = useContext(AuthContext)
    const [token] = useLocalStorage('token')

    useEffect(() => {
        if (!token) {
            dispatch({ type: SET_UNAUTHORIZED })
            return
        }

        dispatch({ type: LOADING })

        doFetch({ method: 'GET', token })
    }, [token, dispatch, doFetch])

    useEffect(() => {
        if (!response) {
            return
        }

        dispatch({ type: SET_AUTHORIZED, payload: response.user })
    }, [response, dispatch])

    return children
}

export default React.memo(CurrentUserChecker)
