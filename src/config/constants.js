export const URL = 'http://crm.vk-da.ru:5005'
// export const URL = 'http://localhost:5005'
export const API_URL = `${URL}/api`

export const APP_LOCAL_TOKEN = 'VKDALocalAppState'

export const DRAWERWIDTH = 240

export const DISH_CATEGORY_TITLE = 'Блюда'
