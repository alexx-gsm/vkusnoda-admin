import React, { useReducer, createContext } from 'react'

export const LOADING = 'LOADING'
export const SET_AUTHORIZED = 'SET_AUTHORIZED'
export const SET_UNAUTHORIZED = 'SET_UNAUTHORIZED'

const initialState = {
    isLoading: false,
    isLoggedIn: null,
    currentUser: null,
    token: null,
}

const reducer = (state, action) => {
    const { type, payload } = action
    switch (type) {
        case LOADING:
            return { ...state, isLoading: true }
        case SET_AUTHORIZED:
            return {
                ...state,
                isLoading: false,
                isLoggedIn: true,
                currentUser: payload,
            }
        case SET_UNAUTHORIZED:
            return {
                ...state,
                isLoading: false,
                isLoggedIn: false,
                currentUser: {},
            }
        default:
            return state
    }
}

export const AuthContext = createContext()

export const AuthContextProvider = ({ children }) => (
    <AuthContext.Provider value={useReducer(reducer, initialState)}>
        {children}
    </AuthContext.Provider>
)
