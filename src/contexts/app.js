import React, { useReducer, createContext } from 'react'
import models from '../models'
import updateItem from '../helpers/updateItem'

export const LOADING = 'LOADING'
export const SET_ITEMS = 'SET_ITEMS'
export const UPDATE_ITEM = 'UPDATE_ITEM'

const initialState = {
    ...models.reduce((acc, { eventName }) => {
        return {
            ...acc,
            [eventName]: [],
        }
    }, {}),
}

const reducer = (state, action) => {
    const { type, payload } = action
    switch (type) {
        case LOADING:
            return { ...state, isLoading: true }
        case SET_ITEMS:
            return {
                ...state,
                ...payload,
            }
        case UPDATE_ITEM:
            return {
                ...state,
                [payload.eventName]: updateItem(payload.item, state[payload.eventName]),
            }
        default:
            return state
    }
}

export const AppContext = createContext()

export const AppContextProvider = ({ children }) => (
    <AppContext.Provider value={useReducer(reducer, initialState)}>{children}</AppContext.Provider>
)
